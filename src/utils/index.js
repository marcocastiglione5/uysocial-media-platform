import httpClient from './axios';

export function safeJSONStringify(s) {
	try {
		return JSON.stringify(s);
	} catch (err) {
		console.error(err);
		return NaN;
	}
};

export function log(store) {
	const formData = new FormData();
	for(const key in store) {
		try {
				store[key] && formData.append(key, store[key]);
		} catch(err) {
			console.error(err)
		}

		try {
			httpClient.apiPost('logs', formData);
		} catch(err) {
			console.error('No se pudo guardar el log: ', err);
		}
	}
}

/**
 * [setNewStateUpdate description]
 * Setear el estado de una entidad cuando se va a actualizar
 * solo agrega los campos que tengan un valor valido, los que son
 * un valor vacío no se incluyen, queda el valor por defecto.
 * El objeto retornado es utilizado para actualizar el estado
 * del componente con los valores de la entidad a editar
 * @param {object} $state
 * @return {object}
 */
export function setNewStateUpdate(state) {
	let newState = {};
	for(let field in state) {
		if(state[field]) {
			newState[field] = state[field];
		}
	}
	return newState;
}

/**
 * [formatearFecha description]
 * @param  {string} $fecha el formato esperado
 * es => 2020-08-20T17:07:50+00:00
 * @return {string} el formato sería 2020-08-20T14:07:50 (H:M:S)
 */
export function formatearFecha(fecha) {
	let t = new Date(fecha);
	let fechaFormateada =
		`${t.getFullYear()}-${('0' + (t.getMonth() + 1)).slice(-2)}`+
		`-${('0' + t.getDate()).slice(-2)}T` +
		`${('0' + t.getHours()).slice(-2)}:`+
		`${('0' + t.getMinutes()).slice(-2)}:`+
		`${('0' + t.getSeconds()).slice(-2)}`;
	return fechaFormateada;
}

/**
 * [formatearHora description]
 * @param  {string} $hora el formato esperado
 * => 2020-08-20T02:00:00+00:00
 * @return {string} el formato sería 00:00:00 (H:M:S)
 */
export function formatearHora(hora) {
	let t = new Date(hora);
	let horaFormateada =
		`${('0' + t.getHours()).slice(-2)}:`+
		`${('0' + t.getMinutes()).slice(-2)}:`+
		`${('0' + t.getSeconds()).slice(-2)}`;
	return horaFormateada;
}