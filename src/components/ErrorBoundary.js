import React, { Component } from "react";

/* utils */
import { log } from '../utils/';

class ErrorBoundary extends Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false };
	}

	static getDerivedStateFromError(error) {
		// Actualiza el estado para que el siguiente renderizado muestre la interfaz de repuesto
		console.log('error: ', error);
		return { hasError: true };
	}

	componentDidCatch(error, errorInfo) {
		// Registrar el error en el servicio de reporte de errores del backend
		console.log('error: ', { error, errorInfo });
		log({
			errorStack: error.stack,
			component: 'ErrorBoundary',
			errorMessage: error.message,
			errorInfo: JSON.stringify(errorInfo),
		})
	}

	render() {
		if (this.state.hasError) {
			// Puedes renderizar cualquier interfaz de repuesto
			return <h1>Error de compilación</h1>;
		}

		return this.props.children;
	}
}

export default ErrorBoundary;
