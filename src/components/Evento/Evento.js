import React, {
	lazy,
	Suspense,
	useEffect,
	useState,
	useRef,
	Fragment
} from 'react';
import { useParams, useHistory, useLocation, Link } from 'react-router-dom';

/* Components */
import Loading from '../Loading/Loading';
import ComprarAcceso from './ComprarAcceso';

/* Style */
import cx from 'classnames';
import style from './evento.css';

/* config | constants */
import { ASSETS_URL } from '../../config.js';

/* Actions */
import { setEvento, resetEvento, resetAccessCodeData } from '../../actions/eventoAction';

/* connect */
import connect from '../../context/connect';

const Evento = ({ evento, isFetching, resetEvento, setEvento, resetAccessCodeData }) => {
	const params = useParams();
	const history = useHistory();
	const location = useLocation();
	const _imageRef = useRef(null);
	const _titleRef = useRef(null);
	const [ imageLoaded, setImageLoaded ] = useState(false);
	const [ maxHeightDescription, setMaxHeightDescription ] = useState('');

	useEffect(() => {
		if(!isFetching) {
			setEvento({...params, history});
		}
		return () => {
			resetEvento();
		}
	}, [location])

	useEffect(() => {
		window.addEventListener("resize", handleResize);
		return () => {
			resetAccessCodeData();
			window.removeEventListener("resize", handleResize);
		}
	}, [])

	useEffect(() => {
		if(_imageRef && _imageRef.current) {
				setStyleDescription();
		}
	}, [imageLoaded])

	const handleResize = () => {
		setStyleDescription();
	}

	const handleLoadedImage = (e) => {
		setImageLoaded(true);
	}

	const setStyleDescription = () => {
		const height = Math.abs(_imageRef.current.height - _titleRef.current.clientHeight);
		if(height > 130) {
			setMaxHeightDescription(`${height}px`);
		} else {
			setMaxHeightDescription('');
		}
	}

	let descripcionEvento = [];
	if(evento.descripcion) {
		descripcionEvento = JSON.parse(evento.descripcion);
	}

	return( (evento.isEmpty) ? <Loading/> :
		<div className={cx(
			style.mainContent,
			{ [`${style.sinFuncion}`]: !evento.funciones.length }
		)} >
			<section>
				<img onLoad={handleLoadedImage} ref={_imageRef} src={ASSETS_URL+evento.imagenPrincipal} alt=""/>
			</section>
			<section className={style.infoBanda} >
				<h3 ref={_titleRef} className={style.titulo} >{ evento.nombre }</h3>
				<div style={{maxHeight: maxHeightDescription}} className={style.descripcion} >
					{
						descripcionEvento.map((text, key) => {
							return(
								<p key={key} >{text}</p>
							)
						})
					}
				</div>

				{ (evento.funciones.length) ? null :
					<div className={style.proximamente} >
						<h4>próximamente</h4>
					</div>
				}
			</section>

			{ (!evento.funciones.length) ? null :
				<section>
					<ComprarAcceso evento={evento} />
				</section>
			}
		</div>
	);
}

const mapStateToProps = (store) => ({
	evento: store.evento,
	isFetching: store.evento.isFetching,
});

const mapDispatchToProps = (dispatch) => ({
	resetEvento: () => dispatch(resetEvento()),
	resetAccessCodeData: () => dispatch(resetAccessCodeData()),
	setEvento: (param) => dispatch(setEvento(param, dispatch)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Evento);