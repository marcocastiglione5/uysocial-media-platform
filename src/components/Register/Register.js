import React, {
	useRef,
	useState,
	Fragment,
	useEffect,
} from 'react';
import PropTypes from 'prop-types';
import ReCAPTCHA from 'react-google-recaptcha';

/* Components */
import Select from '../Select';
import Checkbox from '../Checkbox';
import TextInput from '../TextInput';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';

/* Style */
import cx from 'classnames';
import style from './register.css';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
}));

/* context */
import connect from '../../context/connect';

/* utils */
import validate from './validate';
import toast from '../../utils/toast';
import paises from '../../utils/paises';
import httpClient from '../../utils/axios';
import serealizeData from './serealizeData';
import { SITE_KEY_RECAPTCHA, ASSETS_URL } from '../../config';


/* Actions */
import { verifySession } from '../../actions/loginAction';

const initialState = {
	email: '',
	genero: '',
	nacDia: '',
	nacMes: '',
	nacAnho: '',
	nombres: '',
	telefono: '',
	password: '',
	apellidos: '',
	domicilio: '',
	direccion: '',
	provincia: '',
	localidad: '',
	image_file: '',
	cod_pais: 'ARG',
	recaptcha: null,
	imagen_perfil: '',
	imagen_avatar: '',
	codigo_postal: '',
	aceptoTerminos: 0,
	nro_documento: '',
	acepta_noticias: 1,
	fecha_nacimiento: '',
	email_confirmado: '',
	tipo_documento: 'CI',
	passwordConfirmado: '',
}

const Register = ({
	user,
	renderSidenav,
	verifySession,
	renderCaptcha,
	handleToggleAccount,
}) => {
	const _form = useRef(null);
	const classes = useStyles();
	const _recaptcha = useRef(null);
	const _imageFileRef = useRef(null);
	const _wrapperRegister = useRef(null);
	const [ avatars, setAvatars ] = useState([]);
	const [ state, setState ] = useState(initialState);
	const [ profileImg, setProfileImg ] = useState(null);
	const [openModal, setOpenModal] = React.useState(false);
	const [ disablebRegistro, setDisablebRegistro ] = useState(true);
	const [ requiredCompletedCount, setRequiredCompletedCount ] = useState(0);
	const [ requiredCompletedTotal, setRequiredCompletedTotal ] = useState(6);
	const [ fieldRequired, setFieldRequired ] = useState({
		email: false,
		nombres: false,
		password: false,
		apellidos: false,
		recaptcha: false,
		nro_documento: false,
	});

	useEffect(() => {
		httpClient.apiGet('avatars')
		.then(({ data }) => {
			setAvatars(data);
		})
	}, [])

	useEffect(() => {
		let count = 0;
		for(const field in fieldRequired) {
			if(fieldRequired[field]) count++;
		}

		setDisablebRegistro(count < requiredCompletedTotal || !state.aceptoTerminos)
		setRequiredCompletedCount(count);
	}, [fieldRequired])

	useEffect(() => {
		setState((prevState) => {
			let newState = { ...initialState };
			if(user) {
				for(const field in user) {
					if(user[field] && newState.hasOwnProperty(field)) {
						newState[field] = user[field];
					}
				}
				setProfileImg(null);
				_imageFileRef.current.value = '';
				newState.genero = `${newState.genero}`;
				newState.email_confirmado = newState.email;
				newState.acepta_noticias = user.acepta_noticias;
				if(newState.fecha_nacimiento) {
					const [ anho, mes, dia ] = newState.fecha_nacimiento.split('-');
					newState.nacMes = `${mes}`;
					newState.nacDia = `${dia}`;
					newState.nacAnho = `${anho}`;
				}
			} else {
				newState = { ...initialState };
			}
			return newState;
		})
	}, [user]);

	const onChangeRecaptcha = (e) => {
		setFieldRequired((prevState) => {
			const newState = { ...prevState };
			newState.recaptcha = e != '' && e != null;
			return newState;
		})

		setState((prevState) => {
			const newState = { ...prevState };
			newState.recaptcha = e;
			return newState;
		});

	}

	const handleInput = (e) => {
		var newState = {};
		const nameInput = e.target.name;
		let valueInput = e.target.value;
		switch(e.target.type) {
			case 'file':
				valueInput = e.target.files[0];
				if(valueInput) {
					const objectURL = URL.createObjectURL(valueInput);
					const img = new Image();

					img.onload = function() {
						const { width, height } = this;
						_wrapperRegister.current.scrollTop = 0;
						setProfileImg(objectURL);
						newState.imagen_perfil = '';
						newState.imagen_avatar = '';
					};

					img.onerror = function() {
							console.error('not a valid file: ' + file.type);
					};
					img.src = objectURL;
				} else {
					setProfileImg(null);
				}
				break;
			case 'checkbox':
				valueInput = e.target.checked ? 1 : 0;
				break;
		}

		switch(nameInput) {
			case 'email':
			case 'nacDia':
			case 'nacMes':
			case 'nombres':
			case 'password':
			case 'apellidos':
			case 'recaptcha':
			case 'nro_documento':
				setFieldRequired((prevState) => {
					const newState = { ...prevState };
					newState[nameInput] = valueInput != '';
					return newState;
				})
				break;
			case 'aceptoTerminos':
				setDisablebRegistro(requiredCompletedCount < requiredCompletedTotal || !valueInput)
				break;
			case 'imagen_avatar':
				newState.image_file = null;
				_imageFileRef.current.value = '';
				newState.imagen_perfil = valueInput;
				setProfileImg(ASSETS_URL+avatars.find((el) => el.id == valueInput).avatar_url);
				break;
		}
		setState((prevState) => {
			newState = { ...prevState, ...newState };
			newState[nameInput] = valueInput
			return newState;
		});
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		if(disablebRegistro && !user) return;
		setDisablebRegistro(true);

		toast({
			html: `Se está procesando su ${(user) ? 'actualización' : 'registro'}...`,
			classes:'blue black-text'
		});

		const update = (user) ? true : false;
		const [totalErrors, errors] = validate(state, update);

		if(!totalErrors) {
			try {
				const URI = (update) ? 'actualizar/'+user.id : 'registrar';
				const { data } = await httpClient.apiPost(`clientes/${URI}`, serealizeData(state, update));

				if(data.cod == 400) {
					for(const field in data.errors) {
						for(const err in data.errors[field]) {
							toast({
								html: data.errors[field][err],
								classes:`black-text yellow`
							});
						}
					}
				} else if(data.cod == 200) {
					setState(initialState);
					setProfileImg(null);
					_imageFileRef.current.value = null;
					toast({
						html: `${(update) ? 'Actualización exitosa' : 'Registro exitoso'}`,
						classes:`black-text green`
					});

					if(!update) {
						toast({
							html: 'Revise su bandeja de mail y confirme su registro',
							classes:`black-text green`
						});
					} else {
						verifySession();
					}
					handleToggleAccount(null, true);
				}

				setDisablebRegistro(false);
			} catch(err) {
				console.error(err);
				toast({
						html: 'Ocurrió un error. Por favor intente nuevamente',
						classes:'red black-text'
					});
				setDisablebRegistro(false);
			}
		} else {
			for(const err in errors) {
				toast({
					html: errors[err],
					classes:'red black-text'
				});

			}
			setDisablebRegistro(false);
		}
	}

	const renderAvatarsSelect = () => {
		return( (avatars.length) ?
			<Select
				s={12}
				name='imagen_avatar'
				onChange={handleInput}
				value={state.imagen_avatar}
				selectClassName={style.selectM}
			>
			<option disabled value="" > Elegir un Avatar</option>
			{
				avatars.map((el) => (
					<option
						key={el.id}
						value={el.id}
						data-icon={ASSETS_URL+el.avatar_url}
					> { el.nombre } </option>
				))
			}
			</Select>
			: null
		)
	}

	const handleOpenModal = () => {
		setOpenModal(true);
	};

	const handleCloseModal = () => {
		setOpenModal(false);
	};

	return(
		<div
			ref={_wrapperRegister}
			className={cx(
				style.wrapperRegister,
				{[`${style.mobile}`]: renderSidenav},
			)}
		>
			<div className={style.header} >
				<div className={cx(style.profileImg)} >
					{ (profileImg) ?
							<span
								onClick={handleOpenModal}
								className={style.previewImg}
								style={{backgroundImage: `url(${profileImg})`}}
							></span>
							: (user && user.imagen_perfil) ? // else if
							<span
								onClick={handleOpenModal}
								className={style.previewImg}
								style={{backgroundImage: `url(${ASSETS_URL+user.imagen_perfil})`}}
							></span>
							: // else
							<span
								onClick={handleOpenModal}
								className={style.uploadAvatar}
							><img src="/img/icons/camera.svg" alt=""/></span>
					}
					<Modal
						open={openModal}
						className={classes.modal}
						onClose={handleCloseModal}
						aria-labelledby="simple-modal-title"
						aria-describedby="simple-modal-description"
					>
						<Fade in={openModal}>
							<div className={style.contentModal}>
								{ renderAvatarsSelect() }
								<span
									className={cx('btn', style.btnUpload)}
									onClick={() => { _imageFileRef.current.click() }}
								>
									subir una foto
								</span>

								<button
									className={cx('btn', 'red', style.btnUpload)}
									onClick={() => {
										setProfileImg(null);
										setState((prevState) => ({
											...prevState,
											image_file: '',
											imagen_perfil: '',
											imagen_avatar: '',
										}));
									}}
								>
									borrar
								</button>

								<div onClick={handleCloseModal} className={style.cerrarModal} >
									<span>cerrar</span>
								</div>
						</div>
						</Fade>
					</Modal>
				</div>

				<div className={style.title} >
					<h2>
						{(user) ? 'Actualizar mi perfil' : 'Ingresa tus datos'}
					</h2>
					<div className={style.lineTitle} ></div>
				</div>
				<input
					type="file"
					accept='image/*'
					name='image_file'
					ref={_imageFileRef}
					onChange={handleInput}
					style={{ display: "none" }}
				/>
			</div>

			<form ref={_form} onSubmit={handleSubmit} >
				<div className="row">
					<div className="col s12 l4">
						<TextInput
							s={12}
							validate
							type='text'
							name='nombres'
							required={!user}
							value={state.nombres}
							onChange={handleInput}
							icon={(renderSidenav) ? null : 'account_circle'}
							globalClasses={cx(style.inputIcon, style.inputBottom)}
							label={
								<span>Nombre <span className="red-text">*</span></span>
								}
						/>

						<TextInput
							s={12}
							type='text'
							name='apellidos'
							value={state.apellidos}
							onChange={handleInput}
							icon={(renderSidenav) ? null : 'account_circle'}
							globalClasses={cx(style.inputIcon, style.inputBottom)}
							label={
								<span>Apellidos <span className="red-text">*</span></span>
							}
							// validate
							required={!user}
						/>

						<Select
								label='Tipo de documento'
								m={6}
								s={12}
								name='tipo_documento'
								onChange={handleInput}
								value={state.tipo_documento}
							>
								<option value="CI">Cédula Identidad</option>
								<option value="DNI">DNI</option>
								<option value="PAS">Pasaporte</option>
						</Select>

						<TextInput
							m={6}
							s={12}
							type='text'
							name='nro_documento'
							onChange={handleInput}
							value={state.nro_documento}
							globalClasses={cx(style.inputIcon, style.inputBottom)}
							label={
								<span>Nº documento <span className="red-text">*</span></span>
							}
							validate
							required={!user}
						/>

						{renderSidenav && <div className="col s12"><p className={style.label} >Fecha de nacimiento</p></div>}

						<TextInput
							s={4}
							name='nacDia'
							value={state.nacDia}
							type='text'
							globalClasses={cx(style.inputIcon, style.inputBottom)}
							icon={(renderSidenav) ? null : 'cake'}
							onChange={handleInput}
							label='Día'
						/>

						<TextInput
							s={4}
							name='nacMes'
							value={state.nacMes}
							globalClasses={cx(style.inputBottom)}
							type='text'
							onChange={handleInput}
							label='Mes'
						/>

						<TextInput
							s={4}
							name='nacAnho'
							value={state.nacAnho}
							globalClasses={cx(style.inputBottom)}
							type='text'
							onChange={handleInput}
							label='Año'
						/>

						<Select
							s={12}
							name='genero'
							value={state.genero}
							onChange={handleInput}
						>
							<option disabled value="" >
								Genero
							</option>
							<option value="masculino">Masculino</option>
							<option value="femenino">Femenino</option>
							<option value="otro">Otro</option>
						</Select>
					</div>

					<div className="col s12 l4">
						<TextInput
							s={12}
							name='email'
							value={state.email}
							type='email'
							globalClasses={style.inputIcon}
							icon={(renderSidenav) ? null : 'email'}
							onChange={handleInput}
							label={
								<span>e-mail <span className="red-text">*</span></span>
							}
							validate
							required={!user}
						/>

						<TextInput
							s={12}
							validate
							type='email'
							required={!user}
							name='email_confirmado'
							onChange={handleInput}
							value={state.email_confirmado}
							icon={(renderSidenav) ? null : 'email'}
							globalClasses={cx(style.inputBottom, style.inputIcon)}
							label={
								<span>Confirme e-mail {(user) ? '' : <span className="red-text">*</span>}</span>
							}
						/>

						<TextInput
							s={12}
							type='text'
							name='telefono'
							value={state.telefono}
							onChange={handleInput}
							label='Teléfono/Celular'
							icon={(renderSidenav) ? null : 'local_phone'}
							globalClasses={cx(style.inputIcon, style.inputBottom)}
						/>

						{ (!user) ? null :
							<p className={style.texto} >Dejar en blanco si no desea cambiar su contraseña</p>
						}

						<TextInput
							s={12}
							name='password'
							value={state.password}
							type='password'
							onChange={handleInput}
							label={
								(user) ? 
								<span><span>Nueva contraseña </span><span className="red-text">*</span></span> :
								<span>Contraseña <span className="red-text">*</span></span>
							}
							validate
							required={!user}
						/>

						<TextInput
							s={12}
							validate
							type='password'
							required={!user}
							onChange={handleInput}
							name='passwordConfirmado'
							value={state.passwordConfirmado}
							globalClasses={cx(style.inputBottom)}
							label={
								<span>Confirmar contraseña <span className="red-text">*</span></span>
							}
						/>
					</div>

					<div className="col s12 l4">
						<TextInput
							s={12}
							validate
							type='text'
							required={!user}
							name='domicilio'
							label='Dirección'
							onChange={handleInput}
							value={state.domicilio}
							globalClasses={cx(style.inputBottom)}
						/>

						<TextInput
							s={12}
							validate
							type='text'
							required={!user}
							name='codigo_postal'
							label='Código Postal'
							onChange={handleInput}
							value={state.codigo_postal}
							globalClasses={cx(style.inputBottom)}
						/>

						<Select
							label='País'
							s={12}
							name='cod_pais'
							onChange={handleInput}
							value={state.cod_pais}
						>
							<option disabled value="">Elija un país</option>
						{
							paises.map((pais, index) => (<option key={index} value={pais.code}>{pais.label}</option>))
						}
						</Select>

						<TextInput
							s={12}
							name='provincia'
							value={state.provincia}
							globalClasses={style.inputBottom}
							type='text'
							onChange={handleInput}
							label='Provincia'
						/>

						<TextInput
							s={12}
							name='localidad'
							value={state.localidad}
							globalClasses={style.inputBottom}
							type='text'
							onChange={handleInput}
							label='Localidad'
						/>
					</div>
				</div>
				
				<div className="row" style={{marginBottom: 0}} >
					<div className={cx('col', 's12', 'l8', style.checkboxWrapper)} >
					{ (user) ? null :
						<Fragment>
							<div className="col s12">
								<Checkbox
									name='aceptoTerminos'
									onChange={handleInput}
									value={state.aceptoTerminos}
									filledIn
									id='checkbox-aceptoTerminos'
									label={
										<Fragment>
											Si, he leído y acepto las{' '}
											<a href="#">políticas de privacidad</a>{' '}
											y los <a href="#">términos de uso.</a>
										</Fragment>
									}
								/>
							</div>
						</Fragment>
					}
					<div className="col s12">
						<Checkbox
							name='acepta_noticias'
							onChange={handleInput}
							value={state.acepta_noticias}
							filledIn
							id='checkbox-acepta_noticias'
							label='Suscribirse a nuestro newsletter'
						/>
					</div>

					</div>
					<div className="col s12 l4">
					{ renderCaptcha &&
						<ReCAPTCHA
							ref={_recaptcha}
							className={style.recaptcha}
							size={(renderSidenav) ? 'compact' : 'normal'}
							sitekey={SITE_KEY_RECAPTCHA}
							onChange={onChangeRecaptcha}
						/>
					}
						<button
							disabled={disablebRegistro && !user}
							className={cx('btn', 'btn-primary', 'right', style.btnRegister)}
							onClick={handleSubmit}
						>
							{ (user) ? 'Actualizar' : 'REGISTRARME' }
						</button>
					</div>
				</div>
			</form>
		</div>
	)
}

Register.propTypes = {
	/*
	 * [renderCaptcha description]
	 * Determina si se debe renderizar el CAPTCHA
	*/
	renderCaptcha: PropTypes.bool,
	/**
	 * [renderSidenav description]
	 * Determina si el Register está siendo renderizado dentro del sidebar
	 */
	renderSidenav: PropTypes.bool,
}

const mapStateToProps = (store) => ({
	user: store.login.user,
});

const mapDispathToProps = (dispath) => ({
	verifySession: () => verifySession(dispath)
});

export default connect(mapStateToProps, mapDispathToProps)(Register);