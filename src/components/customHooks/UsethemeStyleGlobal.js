import { makeStyles } from '@material-ui/core/styles';
const firstColor = '#f19122';
const thirdColor = '#7c4d95';
const secondColor = '#d00e5a';

const globalFiled = {
	'& .MuiFormLabel-root.Mui-focused': {
		color: thirdColor
	},
	'& .MuiInputBase-root.Mui-focused fieldset': {
		borderColor: thirdColor
	},
	'& input': {
		border: 'none !important',
		padding: '0 10px !important',
	},
	'& input:focus': {
		boxShadow: 'none !important',
	},
}

const useStyles = makeStyles(theme => ({
	textField: {
		...globalFiled,
	},
	selectField: {
		...globalFiled,
	},
	uploadFile: {
		'&.MuiButton-contained': {
			width: '100%',
			minHeight: '3.5rem',
			color: 'rgba(0, 0, 0, 0.23)',
			backgroundColor: 'transparent',
			border: '1px solid rgba(0, 0, 0, 0.23)',
		}
	}
}))

export default useStyles;