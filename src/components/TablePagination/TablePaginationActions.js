import React from 'react';
import PropTypes from 'prop-types';

/* style */
import UseStyle from './UseStyle';

/* components */
import IconButton from '@material-ui/core/IconButton';
import LastPageIcon from '@material-ui/icons/LastPage';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const TablePaginationActions = (props) => {
	const classes = UseStyle();
	const { count, page, rowsPerPage, onChangePage } = props;

	const handleFirstPageButtonClick = (event) => {
		onChangePage(event, 0);
	};

	const handleBackButtonClick = (event) => {
		onChangePage(event, page - 1);
	};

	const handleNextButtonClick = (event) => {
		onChangePage(event, page + 1);
	};

	const handleLastPageButtonClick = (event) => {
		onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
	};

	return (
		<div className={classes.tablePaginationActions}>
			<IconButton
				disabled={page === 0}
				aria-label='Primera página'
				onClick={handleFirstPageButtonClick}
			>
				<FirstPageIcon />
			</IconButton>

			<IconButton
				disabled={page === 0}
				aria-label="Página anterior"
				onClick={handleBackButtonClick}
			>
				<KeyboardArrowLeft />
			</IconButton>

			<IconButton
				aria-label="Siguiente página"
				onClick={handleNextButtonClick}
				disabled={page >= Math.ceil(count / rowsPerPage) - 1}
			>
				<KeyboardArrowRight />
			</IconButton>

			<IconButton
				aria-label='Última página'
				onClick={handleLastPageButtonClick}
				disabled={page >= Math.ceil(count / rowsPerPage) - 1}
			>
				<LastPageIcon />
			</IconButton>
		</div>
	);
}

TablePaginationActions.propTypes = {
	page: PropTypes.number.isRequired,
	count: PropTypes.number.isRequired,
	onChangePage: PropTypes.func.isRequired,
	rowsPerPage: PropTypes.number.isRequired,
};

export default TablePaginationActions;