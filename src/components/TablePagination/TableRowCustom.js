import React from 'react';
import PropTypes from 'prop-types';
import TableRow from '@material-ui/core/TableRow';

/* style */
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';

const UseStyles = makeStyles(theme => ({
	cebra: {
		'&:nth-of-type(odd)' : {
			backgroundColor: theme.palette.action.hover,
		}
	},
	disableBorder: {
		border: 'none'
	}
}))

const TableRowCustom = ({ disableBgOdd, disableBorder, ...props }) => {
	const classes = UseStyles();

	return(
		<TableRow
			className={cx(
				{ [`${classes.cebra}`] : !disableBgOdd },
				{ [`${classes.disableBorder}`] : disableBorder }
			)}
			{...props}
		/>
	)
}

TableRowCustom.propTypes = {
	/**
	 * [disableBgOdd description]
	 * Deshabilitar el background-color para filas impares
	 */
	disableBgOdd: PropTypes.bool,
}

export default TableRowCustom;