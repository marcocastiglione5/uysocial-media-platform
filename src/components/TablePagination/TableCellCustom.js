import React from 'react';
import TableCell from '@material-ui/core/TableCell';

/* style */
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';

const UseStyles = makeStyles(theme => ({
	default: {
		'&.MuiTableCell-head': {
			backgroundColor: theme.palette.common.black,
			color: theme.palette.common.white,
		}
	},
	disableBorder: {
		'&.MuiTableCell-body': {
			border: 'none'
		}
	}
}))

const TableCellCustom = ({ disableBorder, ...props }) => {
	const classes = UseStyles();
	return(
		<TableCell
			className={cx(
				classes.default,
				{ [`${classes.disableBorder}`] : disableBorder }
			)}
			{...props}
		/>
	)
}

export default TableCellCustom;