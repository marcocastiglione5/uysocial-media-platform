import { makeStyles } from '@material-ui/core/styles';

// Paleta de colores de oficial
const firstColor = '#f19122';
const thirdColor = '#7c4d95';
const secondColor = '#d00e5a';

const UseStyles = makeStyles(theme => ({
	tablePagination: {
		// minWidth: 500,
	},
	tablePaginationActions: {
		flexShrink: 0,
		marginLeft: theme.spacing(2.5),
	},
	tdBtnActions: {
		display: 'flex !important'
	}
}))

export default UseStyles;