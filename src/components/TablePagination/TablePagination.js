import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

/* style */
import cx from 'classnames';
import UseStyle from './UseStyle';

/* components */
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableRowCustom from './TableRowCustom';
import EditIcon from '@material-ui/icons/Edit';
import TableCellCustom from './TableCellCustom';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import IconButton from '@material-ui/core/IconButton';
import TableFooter from '@material-ui/core/TableFooter';
import TablePaginationActions from './TablePaginationActions';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';

const TablePaginationCustom = ({
	rowHead,
	rowBody,
	colSpan,
	editable,
	headProps,
	emptyValue,
	emptyStyle,
	tableClasses,
	activateEmptyRows,
}) => {
	const classes = UseStyle();
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(5);

	const emptyRows = rowsPerPage - Math.min(rowsPerPage, rowBody.length - page * rowsPerPage);

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	return (
		<TableContainer component={Paper}>
			<Table className={cx(classes.tablePagination, tableClasses)}>
			{ (rowHead) ?
				<TableHead>
					<TableRow>
						{ rowHead.map((row, key) => (
								<TableCellCustom
									key={key}
									align="center"
									{ ...headProps }
								>{ row.label }</TableCellCustom>
							))
						}
						{ (!editable) ? null :
							<TableCellCustom align='center' { ...headProps } >
								Acciones
							</TableCellCustom>
						}
					</TableRow>
				</TableHead>
				: null
			}

				<TableBody>
					{(rowsPerPage > 0
						? rowBody.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
						: rowBody
					).map((row) => {
						let contentBody = [];
						let count = 0;
						for(const field in row) {
							if(field != 'id') {
								const content = (
									<TableCellCustom
										scope="row"
										align="center"
										disableBorder={emptyStyle}
										key={`TableCellCustom-${field}`}
									>
										{row[field] || emptyValue}
									</TableCellCustom>
								);

								if(rowHead) {
									let positionField = rowHead.findIndex((head) => head.id == field);
									if(positionField != -1) {
										contentBody[positionField] = content;
									}
								} else {
									contentBody.push(content);
								}
							}
						}

						if(editable) {
							contentBody.push(
								<TableCellCustom
									align='center'
									scope="row-actions"
									key='TableCellCustom-actions'
									className={classes.tdBtnActions}
								>
									<IconButton
										color='inherit'
										size={'medium'}
										disabled={false}
										onClick={row.update}
									>
										<EditIcon/>
									</IconButton>

									<IconButton
										color='inherit'
										size={'medium'}
										disabled={false}
										onClick={row.delete}
									>
										<DeleteIcon/>
									</IconButton>

								</TableCellCustom>
							)
						}

						return(
							<TableRowCustom
								key={row.id}
								disableBgOdd={emptyStyle}
								disableBorder={emptyStyle}
							>
								{ contentBody }
							</TableRowCustom>
						)
					})}

					{/*
						Cuando se está en la última página se deja un espaciado
						del tamaño de la cantidad de filas vacías entre los registros
						y la páginación para hacer referencia a que se llegó al final de los registros
					*/}
					{(activateEmptyRows && emptyRows > 0) && (
						<TableRow style={{ height: 53 * emptyRows }}>
							<TableCell colSpan={(colSpan) ? colSpan : (rowHead) ? rowHead.length : 1000} />
						</TableRow>
					)}
				</TableBody>

				{(rowBody.length > 5) &&
					<TableFooter>
						<TableRow>
							<TablePagination
								page={page}
								count={rowBody.length}
								rowsPerPage={rowsPerPage}
								onChangePage={handleChangePage}
								labelRowsPerPage='Filas por página'
								ActionsComponent={TablePaginationActions}
								onChangeRowsPerPage={handleChangeRowsPerPage}
								colSpan={(colSpan) ? colSpan : (rowHead) ? rowHead.length : 1000}
								rowsPerPageOptions={[5, 10, 25, 50, { label: 'Todos los registros', value: -1 }]}
								SelectProps={{
									inputProps: {
										'aria-label': 'Filas por página',
									},
								}}
								labelDisplayedRows={(l) => {
									return `${l.from} - ${l.to} de ${l.count} / Página ${l.page + 1}`;
								}}
							/>
						</TableRow>
					</TableFooter>
				}
			</Table>
		</TableContainer>
	);
}

TablePaginationCustom.propTypes = {
	/**
	 * [headProps description]
	 * agregar propiedades personalizadas a
	 * <TableCellCustom/> que renderiza el head de la tabla
	 */
	headProps: PropTypes.object,
	/**
	 * [rowBody description]
	 * Datos para ser renderizados en el body, deben tener una propiedad id
	 */
	rowBody: PropTypes.arrayOf(PropTypes.object).isRequired,
	/**
	 * [rowHead description]
	 * Títulos de la cabecera de la tabla para identificar los datos listados
	 */
	rowHead: PropTypes.arrayOf(PropTypes.object),
	/**
	 * [colSpan description]
	 * cantidad de columnas que debe ocupar la celda de páginanción
	 */
	colSpan: PropTypes.number,
	/**
	 * [editable description]
	 * Activa las la columna para mostrar las acciones de editar o eliminar un registro
	 */
	editable: PropTypes.bool,
	/**
	 * [emptyValue description]
	 * Mensaje a mostrar cuando alguna de las columnas es vacía
	 */
	emptyValue: PropTypes.string,
	/**
	 * [emptyStyle description]
	 * Determina si la tabla no debe tener ningun estilo (toda blanca sin fondo)
	 */
	emptyStyle: PropTypes.bool,
	/**
	 * [emptyRows description]
	 * Determinar si se debe compesar las filas
	 * faltantes al llegar al final de las páginas
	 */
	activateEmptyRows: PropTypes.bool
};

TablePaginationCustom.defaultProps = {
	headProps: {},
	editable: false,
	emptyValue: '-',
	activateEmptyRows: true,
}

export default TablePaginationCustom;