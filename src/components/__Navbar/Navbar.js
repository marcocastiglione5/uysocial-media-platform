import React, { useRef, useState, useEffect, Children } from 'react';
import { NavLink, useLocation } from 'react-router-dom';

/* style */
import './sidenav.scss';
import cx from 'classnames';
import style from './navbar.css';

/* components */
import Icon from '../Icon';
import List from '@material-ui/core/List';
import Register from '../Register/Register';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

/* utils */
import idgen from '../../utils/idgen';

const useStyles = makeStyles((theme) => ({
	list: {
		color: '#fff',
		maxWidth: 300,
		width: '100vw',
	},
	swipeableDrawer: {
		'& .MuiPaper-root': {
			background: '#000'
		}
	}
}));

const Navbar = ({
	sidenav,
	options,
	children,
	menuIcon,
	navigation,
	classnamesUl,
	classnamesUlMobile
}) => {
	const classes = useStyles();
	const location = useLocation();
	const sidenavRef = useRef(null);
	const [ openSidebar, setOpenSidebar ] = useState(false);
	const [ openRegister, setOpenRegister ] = useState(false);

	const toggleDrawer = (open) => (event) => {
		if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setOpenSidebar(open);
	};

	return(
		<div className={style.navbarContent}>
			<a
				href="#!"
				data-target="mobile-nav"
				onClick={toggleDrawer(true)}
				className={cx('sidenav-trigger', style.menuIcon)}
			>
				{menuIcon}
			</a>
			<ul
				className={cx(style.listLinks, classnamesUl)}
				style={{
					gridTemplateColumns: `repeat(${children.length}, max-content)`,
				}}
			>
				{children}
			</ul>

			{/* Sidebar */}
			<SwipeableDrawer
				anchor='left'
				open={openSidebar}
				onOpen={toggleDrawer(true)}
				onClose={toggleDrawer(false)}
				className={classes.swipeableDrawer}
			>
				<div
					role="mobile-nav"
					className={classes.list}
					onClick={toggleDrawer(false)}
				>
					<List>
						{sidenav}
					</List>
				</div>
			</SwipeableDrawer>
		</div>
	);
}

Navbar.defaultProps = {
	options: {
		edge: 'left',
		draggable: true,
		inDuration: 250,
		outDuration: 200,
		onOpenStart: null,
		onOpenEnd: null,
		onCloseStart: null,
		onCloseEnd: null,
		preventScrolling: true
	},
	menuIcon: <Icon>menu</Icon>
};

export default Navbar;