import React, { useState, useRef } from 'react';
import { Button } from '@material-ui/core';

/* style */
import cx from 'classnames';
import UsethemeStyleGlobal from './customHooks/UsethemeStyleGlobal';

/* components */
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

export const UploadFile = ({ label, refReset, onChange, name }) => {
	const _inputRef = useRef(null);
	const classesGlobal = UsethemeStyleGlobal();
	const [ fileName, setFileName ] = useState('');

	if(refReset)  {
		refReset.current = {
			reset
		}
	}

	const handle = (e) => {
		_inputRef.current.click();
	}

	const handleChange = (e) => {
		onChange(e);

		let valueInput = e.target.files[0];
		let nameTmp = '';
		if(valueInput) {
			let { name } = valueInput;
			if(name.length > 20) {
				nameTmp =
						name.slice(0, 10)
						+ '...' + name.slice(-10);
			} else {
				nameTmp = name;
			}
		}
		setFileName(nameTmp);
	}

	function reset() {
		setFileName('');
		_inputRef.current.value = '';
	}

	return(
		<label>
			<input
				type='file'
				name={name}
				ref={_inputRef}
				onChange={handleChange}
				style={{ display: 'none' }}
			/>

			<Button
				onClick={handle}
				variant='contained'
				startIcon={<CloudUploadIcon />}
				className={classesGlobal.uploadFile}
			>
				{ fileName || label }
			</Button>
		</label>
	)
}

export default UploadFile;