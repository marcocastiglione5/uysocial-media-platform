import React from 'react';

/* Components */
import Preloader from '../Preloader';

/* Style */
import style from './loading.css';

const Loading = () => {
	return(
		<div className={style.loading} >
			<img src="/img/v-logo-grande.png" alt=""/>
			<Preloader
				active
				size="big"
				color="blue"
			/>
		</div>
	)
}

export default Loading;