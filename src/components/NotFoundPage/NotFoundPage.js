import React from 'react';

const NotFoundPage = () => {
	return(
		<h1 className='center'>Página no provista por el Cliente</h1>
	);
}
export default NotFoundPage;