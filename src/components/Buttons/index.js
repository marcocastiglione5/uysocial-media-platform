import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const disabled = {
	'&.Mui-disabled': {
		opacity: 0.7,
		cursor: 'not-allowed',
		'pointer-events': 'auto'
	}
}

const UseStyles = makeStyles(theme => ({
	gradient: {
		color: '#fff',
		background: 'var(--background-gradient)',
		...disabled
	},
	light: {
		color: '#000',
		border: '1px solid #000',
		background: '#fff',
		...disabled
	}
}))

export const ButtonLight = (props) => {
	const classes = UseStyles();
	return(
		<Button
			{...props}
			variant="contained"
			className={classes.light}
		/>
	)
}

export const ButtonGradient = (props) => {
	const classes = UseStyles();
	return(
		<Button
			{...props}
			variant="contained"
			className={classes.gradient}
		/>
	)
}