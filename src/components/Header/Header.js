import React, { useRef, useState, useEffect, Fragment } from 'react';
import { useLocation, Redirect, useHistory, NavLink, Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/* style */
import cx from 'classnames';
import style from './header.css';
import UseStyle from './UseStyle';

/* utils/constants */
import { safeJSONStringify } from '../../utils/';
import { DASHBOARD_URL_ROOT } from '../../config';

/* context */
import connect from '../../context/connect';

/* components */
import Icon from '../Icon';
import Login from '../Login/Login';
import List from '@material-ui/core/List';
import Slide from '@material-ui/core/Slide';
import Buscador from '../Buscador/Buscador';
import Register from '../Register/Register';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Collapse from '@material-ui/core/Collapse';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import CssBaseline from '@material-ui/core/CssBaseline';
import RedesSociales from './RedesSociales/RedesSociales';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';

/* CustomHooks */
import { useMobileDetector } from '../customHooks/';

/* Actions */
import { logout, handleOpenLogin, handleOpenRegister } from '../../actions/loginAction';

const Header = ({
	user,
	evento,
	logout,
	mobileActive,
	handleOpenLogin,
	triggerOpenLogin,
	handleOpenRegister,
	triggerOpenRegister,
	...props
}) => {
	const classes = UseStyle();
	const history = useHistory();
	const location = useLocation();
	const triggerScroll = useScrollTrigger();
	const triggerMobile = useMobileDetector();
	const activeFixed = useScrollTrigger({ threshold: 10 });
	const [ openSidebar, setOpenSidebar ] = useState(false);
	const [ appBarPosition, setAppBarPosition ] = useState('fixed');

	/* Estados de los contenedores de formularios */
	const [ openLogin, setOpenLogin ] = useState(false);
	const [ openRegister, setOpenRegister ] = useState(false);
	const [ openBuscador, setOpenBuscador ] = useState(false);

	useEffect(() => {
		setAppBarPosition(
			(location.pathname != '/' && !activeFixed) ?
				'sticky' : 'fixed'
		)
	}, [ activeFixed, location ])

	useEffect(() => {
		if(user) {
			/*
			 * Asegurar el cierre de todos los formularios cuando
			 * el usuario inicia sesión correctamente
			*/
			handleActionForm(null, true);
		}
	}, [user])

	useEffect(() => {
		toggleDrawer(false)();
		handleActionForm(null, true);
	}, [ safeJSONStringify(location.pathname) ])

	useEffect(() => {
		/*
		 * una vez abierto el [login ó register] cambiar el estado de
		 * [triggerOpenLogin ó triggerOpenRegister] nuevamente a cerrado (false)
		 * para asegurar que si se llama nuevamente permita abrir los formularios
		 * [login ó register] según corresponda
		*/
		if(triggerOpenLogin) {
			handleActionForm('login');
			handleOpenLogin();
		}
		if(triggerOpenRegister) {
			handleActionForm('register');
			handleOpenRegister();
		}

		if(triggerOpenLogin || triggerOpenRegister) {
			toggleDrawer(true)();
		}

	}, [triggerOpenLogin, triggerOpenRegister]);

	useEffect(() => {
		window.addEventListener("scroll", handleScroll);
		return () => window.removeEventListener("scroll", handleScroll);
	}, [ triggerMobile ]);

	const handleScroll = () => {
		if(!triggerMobile) {
			handleActionForm(null, true);
		}
	}

	/**
	 * Controla las acciones que abren o cierran los contenedores del
	 * buscador, login y registro.
	 * @param {Boolean} $forceClose fuerza el cerrar del contenedor abierto
	 * @param {String} $action acción que se desea realizar
	 * las acciones permitidas son:
	 * --------------------------------------------------------------------------
	 * - logout: cierra la sesión de un usuario logueado
	 * - [register, login, buscar]: cambia al estado opuesto (abierto o cerrado)
	 * --------------------------------------------------------------------------
	 * Post condición: nunca estarán abiertos ambos formularios al mismo tiempo
	 */
	const handleActionForm = (action, forceClose = false) => {
		if(forceClose) {
			setOpenRegister(false);
			setOpenLogin(false);
			setOpenBuscador(false);
			return;
		}

		if(action == 'logout') {
			handleActionForm(null, true);
			logout();
			return;
		}
		setOpenRegister((prevState) => action === 'register' && !prevState)
		setOpenLogin((prevState) => action === 'login' && !prevState)
		setOpenBuscador((prevState) => action === 'buscar' && !prevState)
	}

	/*
	 * Construir la navegación correspondiente para el usuario actual
	*/
	var navigation = [];
	var sidenavNavigation = [];

	const navItemBuscador = {
		title: 'Buscador',
		iconImg: '/img/icons/icono-buscar.svg',
		action: (e) => {
			// Evitar que se cierre el sidebar
			e.stopPropagation();
			handleActionForm('buscar');
		}
	}

	const navItemProfile = {
		title: 'Perfil',
		iconImg: '/img/icons/icono-perfil.svg',
		action: (e) => {
			// Evitar que se cierre el sidebar
			e.stopPropagation();
			handleActionForm((user) ? 'register' : 'login', openRegister);
		}
	}

	const navItemLogout = (!user) ? null : {
		title: 'Logout',
		icon: 'exit_to_app',
		action: () => handleActionForm('logout'),
	}

	const classnamesBotonDefault = cx(
		style.navItemBotonDefault,
		{ [`${style.scrollActive}`]: triggerScroll },
		{ [`${style.mobileActive}`]: triggerMobile }
	);

	const navItemDashboad = {
		title: 'Dashboard',
		className: classnamesBotonDefault,
		action: () => history.push(DASHBOARD_URL_ROOT),
	}

	const navItemTicketera = {
		title: 'Mis entradas',
		iconImg: '/img/icons/icono-mis-compras.svg',
		action: () => history.push('/mis-entradas'),
	}

	const contentFormsSidenav = {
		node: (
			<Fragment>
				{ renderBuscador() }
				{ renderRegister() }
				{ renderContentLogin() }
			</Fragment>
		)
	}

	/*
	 * Si el usuario está logueado se Agrega los items correspondientes a su rol.
	 * En caso contrario se muestra la navegación por defecto
	 */
	if(user) {
		const navItemUser = {
			title: user.nombres
		};

		navigation.push(navItemBuscador);

		if(user.permiso_id) {
			navigation.push(navItemDashboad);

			// sidebar admin
			sidenavNavigation.push(
				navItemDashboad,
				navItemBuscador,
				navItemLogout,
			);
		} else {
			navigation.push(navItemTicketera);
			navigation.push(navItemProfile);

			// sidebar cliente
			sidenavNavigation.push(
				navItemTicketera,
				navItemBuscador,
				navItemProfile,
				navItemLogout,
				contentFormsSidenav,
			);
		}
		navigation.push(navItemLogout);
		navigation.push(navItemUser);
	} else {
		navigation = [
			navItemBuscador,
			navItemProfile,
		];

		// sidebar
		sidenavNavigation.push(
			navItemBuscador,
			navItemProfile,
			contentFormsSidenav,
		);
	}

	const menu = navigation.map(prepareNavigation);
	const sidenavMenu = sidenavNavigation.map(prepareNavigation);

	function prepareNavigation(el, key) {
		// cuando no se quiera renderizar el componente se asigna 'null'
		if(!el) return null;

		var label;
		const {
			icon,
			node,
			title,
			iconImg,
			className,
			action = () => {},
		} = el;

		if(node) {
			return(
				<div
					key={key}
					onClick={(e) => {
						// Evitar que se cierre el sidebar
						e.stopPropagation();
					}}
				>
					{ node }
				</div>
			)
		}

		if(iconImg) {
			label =(
				<img
					alt={title}
					src={iconImg}
					className={cx(
						style.iconImg,
						{ [`${style.scrollActive}`]: triggerScroll },
					)}
				/>
			)
		} else if(icon) {
			label = <Icon >{ icon }</Icon>;
		} else {
			label = title;
		}

		return(
			<MenuItem
				key={key}
				title={title}
				onClick={action}
				className={cx(
					className,
					style.link,
					{ [`${style.scrollActive}`]: triggerScroll },
					{ [`${style.mobileActive}`]: triggerMobile },
				)}
			>
				{ label }
			</MenuItem>
		);
	}

	function renderBuscador() {
		return(
			<Collapse
				unmountOnExit
				timeout='auto'
				in={openBuscador}
				className={cx(
					classes.collapseForms,
					classes.collapseCompact,
					{ 'mobile': triggerMobile }
				)}
			>
				<Buscador />
			</Collapse>
		)
	}

	function renderContentLogin() {
		return(
			<Collapse
				unmountOnExit
				timeout='auto'
				in={openLogin}
				className={cx(
					classes.collapseForms,
					classes.collapseCompact,
					{ 'mobile': triggerMobile }
				)}
			>
				<Login
					open={openLogin}
					handleCreateAccount={handleActionForm}
					recaptchaSize={(triggerMobile) ? 'compact' : 'normal'}
				/>
			</Collapse>
		)
	}

	function renderRegister() {
		return( (user && user.permiso_id) ? null :
			<Collapse
				unmountOnExit
				timeout='auto'
				in={openRegister}
				className={cx(
					classes.collapseForms,
					classes.collapseRegister,
					{ 'mobile': triggerMobile }
				)}
			>
				<Register
					renderCaptcha={openRegister}
					renderSidenav={triggerMobile}
					handleToggleAccount={handleActionForm}
				/>
			</Collapse>
		)
	}

	const toggleDrawer = (open) => (event) => {
		if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setOpenSidebar(open);
	};

	return (
		<Fragment>
			<AppBar
				position={appBarPosition}
				className={cx(
					classes.appBar,
					{ [`${classes.appBarWithScroll}`]: triggerScroll }
				)}
			>
				<Toolbar disableGutters={true} >
					<div className={style.contentLogo} >
						<Link to='/'>
							<img
								src={`/img/logo-v.png`}
								alt="Logo oficial"
								className={cx({ [`${style.scrollActive}`]: triggerScroll }, style.logo)}
							/>
						</Link>
					</div>

					{/* Navegación desktop */}
					<Slide appear={false} direction="down" in={!triggerMobile} unmountOnExit>
						<Toolbar classes={{ root: classes.toolbarNavigation }} >
							<div className={style.redesSocialesWrapper} >
								<RedesSociales scrollActive={triggerScroll} />
							</div>

							<div className={style.navigation} >
								{ menu }
								{ renderBuscador() }
								{ renderRegister() }
								{ renderContentLogin() }
							</div>
						</Toolbar>
					</Slide>
					{/* END - Navegación desktop */}

					{/* Navegación mobile */}
					<Slide appear={false} direction="down" in={triggerMobile} unmountOnExit>
						<Toolbar classes={{ root: classes.toolbarNavigationMobile }} >
							<IconButton
								edge='start'
								color='inherit'
								aria-label='menu'
								onClick={toggleDrawer(true)}
							>
								<MenuIcon />
							</IconButton>

							{/* Sidebar */}
							<SwipeableDrawer
								anchor='left'
								open={openSidebar}
								onOpen={toggleDrawer(true)}
								onClose={toggleDrawer(false)}
								className={classes.swipeableDrawer}
							>
								<div
									role="mobile-nav"
									className={classes.list}
									onClick={toggleDrawer(false)}
								>
									<List>
										{ sidenavMenu }
									</List>
								</div>
							</SwipeableDrawer>
							{/* END - Sidebar */}
						</Toolbar>
					</Slide>
					{/* END - Navegación mobile */}
				</Toolbar>
			</AppBar>
			{(location.pathname != '/' && activeFixed) ? <Toolbar/> : null }
		</Fragment>
	);
}

const mapStateToProps = (store) => ({
	evento: store.evento,
	user: store.login.user,
	triggerOpenLogin: store.login.triggerOpenLogin,
	triggerOpenRegister: store.login.triggerOpenRegister,
});

const mapDispathToProps = (dispath, store) => ({
	logout: () => dispath(logout()),
	handleOpenLogin: () => dispath(handleOpenLogin(store)),
	handleOpenRegister: () => dispath(handleOpenRegister(store)),
});

export default connect(mapStateToProps, mapDispathToProps)(Header);