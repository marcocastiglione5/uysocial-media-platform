import { makeStyles } from '@material-ui/core/styles';

// Paleta de colores oficial
const firstColor = '#f19122';
const thirdColor = '#7c4d95';
const secondColor = '#d00e5a';

const UseStyles = makeStyles(theme => ({
	appBar: {
		'&.MuiAppBar-root': {
			padding: '10px 5%',
			backgroundColor: '#000000c2'
		}
	},
	appBarWithScroll: {
		'&.MuiAppBar-root': {
			backgroundColor: '#000'
		}
	},
	collapseForms: {
		right: 0,
		top: '100%',
		background: '#fff',
		position: 'absolute',
		'&.MuiCollapse-entered': {
			overflowY: 'auto',
		}
	},
	toolbarNavigation: {
		width: '100%'
	},
	toolbarNavigationMobile: {
		width: '100%',
		justifyContent: 'flex-end'
	},
	collapseCompact: {
		width: '350px',
		maxHeight: 'calc(100vh - 150px)',
		'&.mobile': {
			width: '100%',
		}
	},
	collapseRegister: {
		width: '90vw',
		maxWidth: '1200px',
		maxHeight: 'calc(100vh - 150px)',
		'&.mobile': {
			width: '100%',
		}
	},
	swipeableDrawer: {
		'& .MuiPaper-root': {
			background: '#000'
		}
	},
	list: {
		color: '#fff',
		maxWidth: 300,
		width: '100vw',
	}
}))

export default UseStyles;