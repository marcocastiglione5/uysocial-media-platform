import React, { useState, useEffect } from 'react';

/* style */
import cx from 'classnames';
import style from './adminLogin.css';

/* utils */
import toast from '../../utils/toast';

/* Context */
import connect from '../../context/connect';

/* actions */
import { setUserAdmin, restartErrorLogin } from '../../actions/loginAction';

/* components */
import Tab from '../Tab';
import Icon from '../Icon';
import Tabs from '../Tabs';
import TextInput from '../TextInput';
import Preloader from '../Preloader';

const AdminLogin = ({
	errorMsj,
	setUserAdmin,
	isLoginLoading,
	restartErrorLogin,
}) => {
	const [ stateLogin, setStateLogin ] = useState({ usuario: '', password: '' });

	useEffect(() => {
		if(errorMsj) {
			toast({ html: errorMsj, classes:'red black-text' });
			restartErrorLogin();
		}
	}, [ errorMsj ])

	const handleForm = (e) => {
		e.preventDefault();

		if(!stateLogin.usuario || !stateLogin.password) {
			toast({
				classes:'red black-text',
				html: 'Debes ingresar tus credenciales'
			})
		} else {
			setUserAdmin(stateLogin);
		}
	}

	const handleInputLogin = (e) => {
		const target = e.target;
		setStateLogin((prevState) => ({
			...prevState,
			[target.name]: target.value
		}))
	}

	return(
		<div className={style.main} >
		<div className={style.content} >
			<Tabs className={cx('z-depth-1', style.tabs)} >
					<Tab title="Iniciar sesión" active className='tab'>
						<div className={cx('card', 'center', style.card)} >
							<div className="card-header">
								<Icon>lock_open</Icon>
								<h3>Iniciar sesión</h3>
							</div>
							<form name='login' onSubmit={handleForm}>
								<TextInput
									value={stateLogin.usuario} type='text'
									onChange={handleInputLogin} label='Usuario'
									icon='email' name='usuario' validate required
								/>
								<TextInput
									value={stateLogin.password} type='password'
									onChange={handleInputLogin} label='Contraseña'
									icon='lock' name='password' validate 
								/>
								{
									(isLoginLoading) ? <Preloader active /> :
										<input className='center btn' type='submit' value='Entrar'/>
								}
							</form>
						</div>
					</Tab>
				</Tabs>
		</div>
		</div>
	);
}

const mapStateToProps = (store) => ({
	errorMsj: store.login.errorMsj,
	isLoginLoading: store.login.isLoginLoading
});

const mapDispatchToProps = (dispatch) => ({
	restartErrorLogin: () => dispatch(restartErrorLogin()),
	setUserAdmin: (param) => dispatch(setUserAdmin(param, dispatch)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AdminLogin);