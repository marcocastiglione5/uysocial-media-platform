/**
 * Serealizar los datos con el formato correcto
 * antes de ser enviados a la API para ser procesados
 * @param  {array} => {object}  $data
 * @param  {Boolean} $update Es true si los datos a envíar son para actualizar una entidad
 * @param  {string} $entidad Entidad que se va a actualizar o crear
 * @return {Object} FormData
 */
const serealizeData = (data, update = false, entidad = '') => {
	let formData = new FormData();

	if(update){
		// update...
		// Lógica que se aplica unicamente al editar

		// Lógica segementada por entidades
		switch (entidad) {
			case 'portal':
				/*
				 * cuando se edita una entidad del portal no se debe envíar
				 * ni evento_id ni seccion_id porque el id en la DB está compuesto
				 * por estos dos campos y no puede ser modificado para mantener
				 * la integridad de los datos
				 */
				data.evento_id = null;
				data.seccion_id = null;
				break;
		}
	} else {
		// create...
		// Lógica que se aplica unicamente al crear
	}
	// EVENTOS / FUNCIONES / PORTAL / CÁMARAS
	// Lógica que se aplica tanto al crear como al editar
	for(const input in data) {
		switch (input) {
			case 'duracion_estimada':
				if(data.duracion_estimada) {
					// el slice es para asegurar el formato correto de la hora 00:00:00 = 8 caracteres
					formData.append(input, `${data.duracion_estimada}:00`.slice(0, 8))
				}
				break;
			case 'descripcion_show_compra':
				if(data.descripcion_show_compra) {
					const desc = data.descripcion_show_compra.split('\n').filter((text) => text);
					formData.append('descripcion_show_compra', JSON.stringify(desc));
				}
				break;
			default:
				try {
						data[input] && formData.append(input, data[input]);
				} catch(err) {
					console.error(err)
				}
		}
	}

	return formData;
}

export default serealizeData;