/* constants */
import { DASHBOARD_URL_ROOT, DASHBOARD_INDEX, ROLES } from '../../config';

export default [
	{
		id: 'enlaces-generales',
		title: 'Enlaces generales',
		type: 'group',
		children: [
			{
				id: 'vision-general',
				title: 'visión general',
				type: 'item',
				url: DASHBOARD_URL_ROOT + DASHBOARD_INDEX,
			},
			{
				id: 'messages',
				title: 'messages',
				type: 'item',
				url: '/messages',
			},
			{
				id: 'logs',
				title: 'logs',
				type: 'item',
				url: '/logs',
			},
			{
				id: 'selfies',
				title: 'selfies',
				type: 'item',
				url: '/selfies',
			},
			{
				id: 'videos',
				title: 'videos',
				type: 'item',
				url: '/videos',
			},
			{
				id: 'moderate-visor',
				title: 'Moderador del visor',
				type: 'item',
				url: '/selfies/moderate',
			}
		]
	},
	{
		id: 'gestor-eventos',
		title: 'eventos',
		type: 'group',
		children: [
			{
				id: 'ver-eventos',
				title: 'Listado de eventos',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'eventos',
			},
			{
				id: 'agregar-evento',
				title: 'Agregar',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'eventos/agregar',
			}
		]
	},
	{
		id: 'gestor-funciones',
		title: 'Funciones',
		type: 'group',
		children: [
			{
				id: 'ver-todas',
				title: 'Listado de funciones',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'funciones',
			},
			{
				id: 'agregar-funciones',
				title: 'Agregar función',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'funciones/agregar',
			}
		]
	},
	{
		id: 'gestor-camaras',
		title: 'Cámaras',
		type: 'group',
		children: [
			{
				id: 'ver-todas',
				title: 'Listado de cámaras',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'camaras',
			},
			{
				id: 'agregar-camara',
				title: 'Agregar cámara',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'camaras/agregar',
			}
		]
	},
	{
		id: 'gestor-portal',
		title: 'Portal',
		type: 'group',
		children: [
			{
				id: 'ver-todas',
				title: 'Listado de secciones',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'portal/secciones',
			},
			{
				id: 'agregar-seccion',
				title: 'Agregar sección',
				type: 'item',
				url: DASHBOARD_URL_ROOT + 'portal/agregar-seccion',
			}
		]
	},
	// {
	// 	id: 'gestor-medios-de-pago',
	// 	title: 'Medios de pago',
	// 	type: 'group',
	// 	children: [
	// 		{
	// 			id: 'ver-medios-de-pago',
	// 			title: 'Ver todos',
	// 			type: 'item',
	// 			url: DASHBOARD_URL_ROOT + 'medios-de-pago',
	// 		},
	// 		{
	// 			id: 'agregar-medio-de-pago',
	// 			title: 'Agregar',
	// 			type: 'item',
	// 			url: DASHBOARD_URL_ROOT + 'medios-de-pago/agregar',
	// 		}
	// 	]
	// }
]