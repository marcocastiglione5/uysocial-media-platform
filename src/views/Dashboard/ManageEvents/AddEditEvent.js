import React, { useRef, useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/*components */
import Loading  from '../Loading';
import Textarea from '../../../components/Textarea';
import { MenuItem, TextField } from '@material-ui/core';
import UploadFile from '../../../components/UploadFile';

/* utils */
import validate from './validateEvent';
import toast from '../../../utils/toast';
import serealizeData from '../serealizeData';
import httpClient from '../../../utils/axios';
import {
	formatearFecha,
	setNewStateUpdate,
} from '../../../utils';

const initialState = {
	nombre: '',
	estado_id: '',
	image_file: '',
	inicio_venta: '',
	nombre_recinto: '',
	ubicacion_recinto: '',
	max_venta_entradas: '',
	ecommerce_productor: '',
	descripcion_show_compra: '',
}

const AddEvent = ({ layaout, history }) => {
	const params = useParams();
	const _uploapFileRef = useRef(null);
	const classes = UsethemeStyleGlobal();
	const [ update, setUpdate ] = useState(false);
	const [ state, setState ] = useState(initialState);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ estadosEvento, setEstadosEvento ] = useState([]);

	// Fecha y duración actual de la evento cuando se está editando
	const [ defaultValueFecha, setDefaultValueFecha ] = useState('');

	useEffect(() => {
		httpClient.apiGet('admin/estados/clase/entidad',
			{ cancelToken: httpClient.cancelToken }
		)
		.then(({ data }) => {
			setEstadosEvento(data);
		})
		.catch((err) => {
			if (axios.isCancel(err)) {
				console.log("cancelled 3", err);
			} else {
				console.error(err);
			}
		})
	}, [])

	useEffect(() => {
		if(params.id) {
			setUpdate(true);
			httpClient.apiGet(`admin/eventos/editar/${params.id}`, {
				cancelToken: httpClient.cancelToken
			})
			.then(({ data }) => {
				if(!data.evento) {
					toast({
						classes:`black-text yellow`,
						html: 'El evento que intenta editar no existe o fue eliminada'
					})

					history.replace(`${DASHBOARD_URL_ROOT}eventos/agregar`);
				} else {
					let fechaFormateada = formatearFecha(data.evento.inicio_venta);
					setDefaultValueFecha(fechaFormateada);

					const desc = JSON.parse(data.evento.descripcion_show_compra).join('\n\n');

					setState((prevState) => ({
						...prevState,
						...setNewStateUpdate(data.evento),
						inicio_venta: fechaFormateada,
						descripcion_show_compra: desc,
					}))
					setUploading(false);
				}
			})
			.catch((err) => {
				if (axios.isCancel(err)) {
					console.log('cancelled 1', err);
				} else {
					console.error(err);
				}
			})
		} else {
			setUploading(false);
		}
	}, [ params ])

	const handleInput = (e) => {
		const nameInput = e.target.name;
		let valueInput = e.target.value;

		switch(nameInput) {
			case 'image_file':
				valueInput = e.target.files[0];
				break;
		}

		setState((prevState) => {
			const newState = { ...prevState };
			newState[nameInput] = valueInput
			return newState;
		});
	};

	const handleSubmit = async () => {
		const [totalErrors, errors] = validate(state, update);

		if(!totalErrors) {
			try {
				const URI = (update) ? 'editar/'+params.id : 'agregar';
				const { data } = await httpClient.apiPost(
					`admin/eventos/${URI}`,
					serealizeData(state, update),
					{ cancelToken: httpClient.cancelToken }
				);

				if(data.cod == 400) {
					for(const field in data.errors) {
						for(const err in data.errors[field]) {
							toast({
								html: data.errors[field][err],
								classes:`black-text yellow`
							});
						}
					}
				} else if(data.cod == 200) {
					_uploapFileRef.current.reset();
					toast({
						html: `${(update) ? 'Actualización exitosa' : 'Registro exitoso'}`,
						classes:`black-text green`
					});

					if(update) {
						const desc = JSON.parse(data.evento.descripcion_show_compra).join('\n\n');
						setState((prevState) => ({
							...prevState,
							...setNewStateUpdate(data.evento),
							inicio_venta: formatearFecha(data.evento.inicio_venta),
							descripcion_show_compra: desc,
						}))
					} else {
						setState(initialState);
						setDefaultValueFecha('');
					}
				}
			} catch(err) {
				console.error(err);
				toast({
					classes: 'black-text red',
					html: 'Algo a ido mal. Por favor vuelve a intentar'
				})
			}
		} else {
			for(const err in errors) {
				toast({
					html: errors[err],
					classes:'red black-text'
				});

			}
		}
	}

	return((uploadingFlag) ? <Loading/> :
		<div className={cx(style.addWrapper)} >
			<h3>
				{ (update) ? 'Editar evento' : 'Crear un nuevo evento' }
			</h3>
			<section className={cx(style.gridC2, style.grid, style.gridInputs)} >
				<TextField
					type='text'
					name='nombre'
					variant='outlined'
					value={state.nombre}
					onChange={handleInput}
					className={cx(classes.textField)}
					label={
						<span>Nombre del evento <span className="red-text">*</span></span>
						}
				/>

				<TextField
					type='text'
					variant='outlined'
					name='nombre_recinto'
					value={state.nombre_recinto}
					onChange={handleInput}
					className={cx(classes.textField)}
					label={
						<span>Nombre del recinto <span className="red-text">*</span></span>
						}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='ubicacion_recinto'
					label='Ubicación del recinto'
					value={state.ubicacion_recinto}
					className={cx(classes.textField)}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='ecommerce_productor'
					label='Ecommerce productor'
					value={state.ecommerce_productor}
					className={cx(classes.textField)}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='ecommerce_productor'
					label='Ecommerce productor'
					value={state.ecommerce_productor}
					className={cx(classes.textField)}
				/>

				<TextField
					type='number'
					variant='outlined'
					onChange={handleInput}
					name='max_venta_entradas'
					value={state.max_venta_entradas}
					className={cx(classes.textField)}
					label='Máximo de ventas por sesión'
				/>

				<TextField
					select
					variant='outlined'
					name='estado_id'
					onChange={handleInput}
					value={state.estado_id}
					label='Estado del evento'
					className={cx(classes.selectField)}
				>
					{
						estadosEvento.map((el) => (
							<MenuItem key={el.id} value={el.id} >{ el.nombre }</MenuItem>
						))
					}
				</TextField>

				<UploadFile
					name='image_file'
					onChange={handleInput}
					refReset={_uploapFileRef}
					label={
						<span>Imagen del evento <span className="red-text">*</span></span>
					}
				/>

				<TextField
					name='inicio_venta'
					type='datetime-local'
					onChange={handleInput}
					label='inicio de venta'
					defaultValue={defaultValueFecha}
					InputProps={{
						classes:{
							root: style.textFieldInput
						}
					}}
					InputLabelProps={{
						shrink: true,
					}}
				/>
			</section>

			<section className={style.description}>
				<Textarea
					s={12}
					onChange={handleInput}
					label='Descripción del evento'
					name='descripcion_show_compra'
					value={state.descripcion_show_compra}
				/>
			</section>

			{/*<section className={style.botonera} >
				<h4>Más opciones para el evento</h4>
				<span className='btn' >
					Agregar funciones
				</span>

				<span className='btn' >
					Agregar artistas
				</span>

				<span className='btn' >
					Agregar medios de pago
				</span>

				<span className='btn' >
					Agregar publicidad
				</span>

				<span className='btn' >
					Agregar banners en el portal web
				</span>
			</section>*/}

			<section className={style.submit}>
				<button
					className={cx('btn')}
					onClick={handleSubmit}
				>
					{ (update) ? 'editar' : 'agregar'}{' '}evento
				</button>
			</section>
		</div>
	);
}
export default AddEvent;