import React, { useState, useEffect } from 'react';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/* utils / config */
import httpClient from '../../../utils/axios';
import { DASHBOARD_URL_ROOT } from '../../../config';

/*components */
import Loading  from '../Loading';
import toast from '../../../utils/toast';
import TablePagination from '../../../components/TablePagination/TablePagination';

const ListEvent = ({ layaout, history }) => {
	const [ eventos, setEventos ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ uploadingDeleteFlag, setUploadingDeleteFlag ] = useState(false);

	useEffect(() => {
		httpClient.apiGet('admin/eventos')
		.then(({ data }) => {
			setEventos(
				data.map((evento) => {
					let desc =
						JSON.parse(evento.descripcion_show_compra).join(' ');
						if(desc.length > 100) {
							desc = desc.slice(0, 100) + '...';
						}
					return {
						...evento,
						descripcion_show_compra: desc,
						update: () => updateEvento(evento.id),
						delete: () => deleteEvento(evento.id),
					}
				})
			);
			setUploading(false);
		})
		return () => {}
	}, [])

	const updateEvento = (id) => {
		history.push(`${DASHBOARD_URL_ROOT}eventos/editar/${id}`);
	}

	const deleteEvento = (id) => {
		setUploadingDeleteFlag(true);
		httpClient.apiPost(`admin/eventos/eliminar/${id}`)
		.then(({ data }) => {
			toast({
				html: data.message,
				classes:`black-text ${(data.cod == 200) ? 'green' : 'yellow'}`
			});
			setEventos((prevState) =>
				prevState.filter((evento) => {
					return evento.id != id;
				})
			)
			setUploadingDeleteFlag(false);
		})
	}

	return(
		<div className={cx(style.addWrapper)} >
			<h3>Listado de eventos</h3>
			{ (uploadingDeleteFlag) ?
					<Loading/> // Se está eliminando una entidad...
				: (eventos.length) ? // No se a creado ninguna cámara aún
					<TablePagination
						editable
						rowBody={eventos}
						rowHead={[
							{
								id: 'nombre',
								label: 'Nombre',
							},
							{
								id: 'nombre_recinto',
								label: 'Nombre del recinto',
							},
							{
								id: 'ubicacion_recinto',
								label: 'Ubicación del recinto',
							},
							{
								id: 'inicio_venta',
								label: 'Inicio de venta',
							},
							{
								id: 'descripcion_show_compra',
								label: 'Descripción',
							},
							{
								id: 'ecommerce_productor',
								label: 'Eccommerce del productor',
							},
							{
								id: 'max_venta_entradas',
								label: 'Máximo de ventas por entradas',
							},
							{
								id: 'estado_id',
								label: 'Estado',
							},
						]}
					/>
				: (uploadingFlag) ? <Loading/> :
					<p className='center' >No se a creado ningun evento aún</p>
			}
		</div>
	)
}

export default ListEvent;