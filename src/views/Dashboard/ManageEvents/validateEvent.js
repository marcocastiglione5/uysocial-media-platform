const emptyString = (value) => (value && value.trim()) ? false : true

const validate = (values, update = false) => {
	const errors = {};
	let totalErrors = 0;

	if(update) {
		// Validaciones para la actualización del evento
	} else {
		// Validaciones para el registro de un nuevo evento
		if (emptyString(values.nombre)) {
			errors.nombre = 'El nombre del evento es requerido';
			totalErrors++;
		}

		if (emptyString(values.nombre_recinto)) {
			errors.nombre_recinto = 'El nombre del recinto es requerido';
			totalErrors++;
		}

		if (emptyString(values.descripcion_show_compra)) {
			errors.descripcion_show_compra = 'La descripción del evento es requerida';
			totalErrors++;
		}

		if(emptyString(values.inicio_venta)) {
			errors.inicio_venta = 'La fecha de inicio de venta es requerida';
			totalErrors++;
		}
	}
	return [totalErrors, errors];
};

export default validate;
