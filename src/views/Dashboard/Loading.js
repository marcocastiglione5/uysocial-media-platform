import React from 'react';

/* style */
import style from './dashboard.css';

/* components */
import Preloader  from '../../components/Preloader';

const Loading = () => {
	return(
		<div className={style.loading}>
			<Preloader
				active
				size="big"
				color="blue"
			/>
		</div>
	)
}

export default Loading;