import React, { useRef, useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/*components */
import Loading  from '../Loading';
import Preloader  from '../../../components/Preloader';
import { MenuItem, TextField } from '@material-ui/core';

/* utils / config */
import validate from './validateCamera';
import toast from '../../../utils/toast';
import serealizeData from '../serealizeData';
import httpClient from '../../../utils/axios';
import { setNewStateUpdate } from '../../../utils';
import { DASHBOARD_URL_ROOT } from '../../../config';

const initialState = {
	marca: '',
	modelo: '',
	cam_der: '',
	cam_izq: '',
	cam_abajo: '',
	cam_arriba: '',
	funcion_id: '',
	altura_camara: '',
	resolucion_id: '',
	tipo_camara_id: '',
	angulo_orientacion: '',
	cam_diag_abajo_izq: '',
	cam_diag_abajo_der: '',
	cam_diag_arriba_izq: '',
	cam_diag_arriba_der: '',
	velocidad_transmision: '',
	coordenadas_espaciales: '',
}


const AddEditCamera = ({ layaout, history }) => {
	const params = useParams();
	const classes = UsethemeStyleGlobal();
	const [ camaras, setCamaras ] = useState([]);
	const [ update, setUpdate ] = useState(false);
	const [ funciones, setFunciones ] = useState([]);
	const [ state, setState ] = useState(initialState);
	const [ tiposCamara, setTiposCamara ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ resolucionesCamara, setResolucionesCamara ] = useState([]);

	useEffect(() => {
		if(params.id) {
			setUpdate(true);
			httpClient.apiGet(`admin/mapa-camaras/editar/${params.id}`, {
				cancelToken: httpClient.cancelToken
			})
			.then(({ data }) => {
				if(!data.camara) {
					toast({
						classes:`black-text yellow`,
						html: 'La cámara que intenta editar no existe o fue eliminada'
					})

					history.replace(`${DASHBOARD_URL_ROOT}camaras/agregar`);
				} else {
					setState((prevState) => ({
						...prevState,
						...setNewStateUpdate(data.camara)
					}))
					setUploading(false);
				}
			})
			.catch((err) => {
				if (axios.isCancel(err)) {
					console.log("cancelled 1", err);
				} else {
					console.error(err);
				}
			})
		} else {
			setUploading(false);
		}
	}, [ params ])

	useEffect(() => {
		httpClient.apiGet('admin/funciones', { cancelToken: httpClient.cancelToken  })
		.then(({ data }) => {
			setFunciones(data);
		})
		.catch((err) => {
			if (axios.isCancel(err)) {
				console.log("cancelled 2", err);
			} else {
				console.error(err);
			}
		})

		httpClient.apiGet('admin/resolucion-camaras', { cancelToken: httpClient.cancelToken })
		.then(({ data }) => {
			setResolucionesCamara(data);
		})
		.catch((err) => {
			if (axios.isCancel(err)) {
				console.log("cancelled 3", err);
			} else {
				console.error(err);
			}
		})

		httpClient.apiGet('admin/tipo-camaras', { cancelToken: httpClient.cancelToken })
		.then(({ data }) => {
			setTiposCamara(data);
		})
		.catch((err) => {
			if (axios.isCancel(err)) {
				console.log("cancelled 4", err);
			} else {
				console.error(err);
			}
		})
		return () => {}
	}, [])

	useEffect(() => {
		if(state.funcion_id) {
			httpClient.apiGet(`admin/mapa-camaras/por-funcion/${state.funcion_id}`, {
				cancelToken: httpClient.cancelToken
			})
			.then(({ data }) => {
				setCamaras(data);
			})
			.catch((err) => {
				if (axios.isCancel(err)) {
					console.log("cancelled 5", err);
				} else {
					console.error(err);
				}
			})
		}
	}, [state.funcion_id])

	const handleInput = (e) => {
		const nameInput = e.target.name;
		let valueInput = e.target.value;

		switch(nameInput) {
			case 'funcion_id':
				setState((prevState) => ({
					...prevState,
					cam_der: '',
					cam_izq: '',
					cam_abajo: '',
					cam_arriba: '',
					cam_diag_abajo_izq: '',
					cam_diag_abajo_der: '',
					cam_diag_arriba_izq: '',
					cam_diag_arriba_der: '',
				}))
				break;
		}

		setState((prevState) => {
			const newState = { ...prevState };
			newState[nameInput] = valueInput
			return newState;
		});
	};

	const handleSubmit = async () => {
		const [totalErrors, errors] = validate(state, update);

		if(!totalErrors) {
			try {
				const URI = (update) ? 'editar/'+params.id : 'agregar';
				const { data } = await httpClient.apiPost(
					`admin/mapa-camaras/${URI}`,
					serealizeData(state, update),
					{ cancelToken: httpClient.cancelToken }
				);

				if(data.cod == 400) {
					for(const field in data.errors) {
						for(const err in data.errors[field]) {
							toast({
								html: data.errors[field][err],
								classes:`black-text yellow`
							});
						}
					}
				} else if(data.cod == 200) {
					toast({
						html: `${(update) ? 'Actualización exitosa' : 'Registro exitoso'}`,
						classes:`black-text green`
					});

					if(update) {
						setState((prevState) => ({
							...prevState,
							...setNewStateUpdate(data.camara)
						}))
					} else {
						setState(initialState);
					}
				}
			} catch(err) {
				if (axios.isCancel(err)) {
					console.log("cancelled 6", err);
				} else {
					console.error(err);
				}
				toast({
					classes: 'black-text red',
					html: 'Algo a ido mal. Por favor vuelve a intentar'
				})
			}
		} else {
			for(const err in errors) {
				toast({
					html: errors[err],
					classes:'red black-text'
				});

			}
		}
	}

	return( (uploadingFlag) ? <Loading/> :
		<div className={cx(style.addWrapper)} >
			<h3>
				{ (update) ? 'Editar cámara' : 'Crear una cámara' }
			</h3>
			<section className={cx(style.gridC2, style.grid, style.gridInputs)} >
				<TextField
					type='number'
					variant='outlined'
					onChange={handleInput}
					name='angulo_orientacion'
					value={state.angulo_orientacion}
					className={cx(classes.textField)}
					label={
						<span>Angulo de orientación <span className="red-text">*</span></span>
						}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='coordenadas_espaciales'
					className={cx(classes.textField)}
					value={state.coordenadas_espaciales}
					label={
						<span>Coordenadas espaciales <span className="red-text">*</span></span>
					}
				/>

				<TextField
					type='number'
					variant='outlined'
					name='altura_camara'
					onChange={handleInput}
					value={state.altura_camara}
					className={cx(classes.textField)}
					label={
						<span>Altura de la cámara (mts) <span className="red-text">*</span></span>
					}
				/>

				<TextField
					type='text'
					name='marca'
					label='Marca'
					variant='outlined'
					value={state.marca}
					onChange={handleInput}
					className={cx(classes.textField)}
				/>

				<TextField
					type='text'
					name='modelo'
					label='Modelo'
					variant='outlined'
					value={state.modelo}
					onChange={handleInput}
					className={cx(classes.textField)}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='velocidad_transmision'
					label={
						<span>Velocidad de transmisión <span className="red-text">*</span></span>
					}
					className={cx(classes.textField)}
					value={state.velocidad_transmision}
				/>

				{ (update && !tiposCamara.length) ? null :
					<TextField
						select
						variant='outlined'
						name='tipo_camara_id'
						label='Tipo de cámara'
						onChange={handleInput}
						value={state.tipo_camara_id}
						className={cx(classes.selectField)}
					>
						{
							tiposCamara.map((el) => (
								<MenuItem key={el.id} value={el.id} >{ el.descripcion }</MenuItem>
							))
						}
					</TextField>
				}

				{ (update && !resolucionesCamara.length) ? null :
					<TextField
						select
						variant='outlined'
						name='resolucion_id'
						onChange={handleInput}
						value={state.resolucion_id}
						label='Resolución de la cámara'
						className={cx(classes.selectField)}
					>
						{
							resolucionesCamara.map((el) => (
								<MenuItem key={el.id} value={el.id} >{ el.descripcion }</MenuItem>
							))
						}
					</TextField>
				}

				{ (update && !funciones.length) ? null :
					<TextField
						select
						variant='outlined'
						name='funcion_id'
						onChange={handleInput}
						value={state.funcion_id}
						label='Función a al que pertenece'
						className={cx(classes.selectField)}
					>
						{
							funciones.map((el) => (
								<MenuItem key={el.id} value={el.id} >{ el.nombre }</MenuItem>
							))
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						name='cam_der'
						value={state.cam_der}
						onChange={handleInput}
						label='Cámara a la derecha'
						className={cx(classes.selectField)}
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						name='cam_izq'
						value={state.cam_izq}
						onChange={handleInput}
						label='Cámara a la izquierda'
						className={cx(classes.selectField)}
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						name='cam_abajo'
						label='Cámara abajo'
						onChange={handleInput}
						value={state.cam_abajo}
						className={cx(classes.selectField)}
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						name='cam_arriba'
						label='Cámara arriba'
						onChange={handleInput}
						value={state.cam_arriba}
						className={cx(classes.selectField)}
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						onChange={handleInput}
						name='cam_diag_abajo_izq'
						value={state.cam_diag_abajo_izq}
						className={cx(classes.selectField)}
						label='Cámara diagonal abajo a la izquierda'
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						onChange={handleInput}
						name='cam_diag_abajo_der'
						value={state.cam_diag_abajo_der}
						className={cx(classes.selectField)}
						label='Cámara diagonal abajo a la derecha'
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						onChange={handleInput}
						name='cam_diag_arriba_izq'
						value={state.cam_diag_arriba_izq}
						className={cx(classes.selectField)}
						label='Cámara diagonal arriba a la izquierda'
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}

				{ (update && !camaras.length) ? null :
					<TextField
						select
						variant='outlined'
						onChange={handleInput}
						name='cam_diag_arriba_der'
						value={state.cam_diag_arriba_der}
						className={cx(classes.selectField)}
						label='Cámara diagonal arriba a la derecha'
					>
						{ (!state.funcion_id) ?
								<MenuItem disabled value="" >Debes elejir primero una función</MenuItem>
							: (camaras.length) ? //else if
								camaras.map((el) => (
									<MenuItem key={el.id} value={el.id} >Cámara - { el.id }</MenuItem>
								))
							:
								<MenuItem disabled value="" >No existen cámaras para la función elegida</MenuItem>
						}
					</TextField>
				}
			</section>

			<section className={style.submit}>
				<button
					className={cx('btn')}
					onClick={handleSubmit}
				>
					{ (update) ? 'editar' : 'crear'}{' '}cámara
				</button>
			</section>
		</div>
	);
}
export default AddEditCamera;