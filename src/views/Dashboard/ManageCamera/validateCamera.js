const emptyString = (value) => (value && value.trim()) ? false : true

const validate = (values, update = false) => {
	const errors = {};
	let totalErrors = 0;

	if(update) {
		// Validaciones para la actualización de la cámara
	} else {
		// Validaciones para el registro de una nueva cámara
		if (emptyString(values.coordenadas_espaciales)) {
			errors.coordenadas_espaciales = 'Las coordenadas espaciales son requeridas';
			totalErrors++;
		}

		if (emptyString(values.angulo_orientacion)) {
			errors.angulo_orientacion = 'El angulo de orientación es requerido';
			totalErrors++;
		}

		if (!values.funcion_id) {
			errors.funcion_id = 'Debes elegir una función';
			totalErrors++;
		}

		if(emptyString(values.altura_camara)) {
			errors.altura_camara = 'La altura es requerida';
			totalErrors++;
		}

		if(emptyString(values.marca)) {
			errors.marca = 'La marca es requerida';
			totalErrors++;
		}

		if(emptyString(values.modelo)) {
			errors.modelo = 'El modelo es requerido';
			totalErrors++;
		}
	}
	return [totalErrors, errors];
};

export default validate;
