import React, { useState, useEffect } from 'react';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/* utils / config */
import httpClient from '../../../utils/axios';
import { DASHBOARD_URL_ROOT } from '../../../config';

/*components */
import Loading  from '../Loading';
import toast from '../../../utils/toast';
import TablePagination from '../../../components/TablePagination/TablePagination';

const ListCamera = ({ layaout, history }) => {
	const [ camaras, setCamaras ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ uploadingDeleteFlag, setUploadingDeleteFlag ] = useState(false);

	useEffect(() => {
		httpClient.apiGet('admin/mapa-camaras')
		.then(({ data }) => {
			setCamaras(data.map((camara) => ({
				...camara,
				update: () => updateCamera(camara.id),
				delete: () => deleteCamera(camara.id),
			})));
			setUploading(false);
		})
		return () => {}
	}, [])

	const updateCamera = (id) => {
		history.push(`${DASHBOARD_URL_ROOT}camaras/editar/${id}`);
	}

	const deleteCamera = (id) => {
		setUploadingDeleteFlag(true);
		httpClient.apiPost(`admin/mapa-camaras/eliminar/${id}`)
		.then(({ data }) => {
			toast({
				html: data.message,
				classes:`black-text ${(data.cod == 200) ? 'green' : 'yellow'}`
			});
			setCamaras((prevState) =>
				prevState.filter((camara) => {
					return camara.id != id;
				})
			)
			setUploadingDeleteFlag(false);
		})
	}

	return(
		<div className={cx(style.addWrapper)} >
			<h3>Listado de cámaras</h3>
			{ (uploadingDeleteFlag) ?
					<Loading/> // Se está eliminando una entidad...
				: (camaras.length) ? // No se a creado ninguna cámara aún
					<TablePagination
						editable
						colSpan={5}
						rowBody={camaras}
						headProps={{
							classes: {
								head: style.TheadListCamera
							}
						}}
						rowHead={[
							{
								id: 'angulo_orientacion',
								label: 'Angulo de orientación',
							},
							{
								id: 'coordenadas_espaciales',
								label: 'Coordenadas espaciales',
							},
							{
								id: 'altura_camara',
								label: 'altura cámara',
							},
							{
								id: 'marca',
								label: 'Marca',
							},
							{
								id: 'modelo',
								label: 'Modelo',
							},
							{
								id: 'cam_abajo',
								label: 'Cámara de abajo',
							},
							{
								id: 'cam_arriba',
								label: 'Cámara de arriba',
							},
							{
								id: 'cam_der',
								label: 'Cámara a la derecha',
							},
							{
								id: 'cam_izq',
								label: 'Cámara a la izqierda',
							},
							{
								id: 'cam_diag_abajo_der',
								label: 'Cámara diagonal abajo a la derecha',
							},
							{
								id: 'cam_diag_abajo_izq',
								label: 'Cámara diagonal abajo a la izquierda',
							},
							{
								id: 'cam_diag_arriba_izq',
								label: 'Cámara diagonal arriba a la izquierda',
							},
							{
								id: 'cam_diag_arriba_der',
								label: 'Cámara diagonal arriba a la derecha',
							},
							{
								id: 'velocidad_transmision',
								label: 'Velocidad de transmisión',
							},
							{
								id: 'resolucion_id',
								label: 'Resolucion',
							},
							{
								id: 'tipo_camara_id',
								label: 'Tipo de cámara',
							},
							{
								id: 'funcion_id',
								label: 'ID de la Función',
							},
						]}
					/>
				: (uploadingFlag) ? <Loading/> :
					<p className='center' >No se a creado ninguna cámara aún</p>
			}
		</div>
	)
}

export default ListCamera;