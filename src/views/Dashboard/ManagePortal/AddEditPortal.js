import React, { useRef, useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import qs from 'querystringify';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/*components */
import Loading  from '../Loading';
import UploadFile from '../../../components/UploadFile';
import { MenuItem, TextField } from '@material-ui/core';

/* utils */
import validate from './validatePortal';
import toast from '../../../utils/toast';
import serealizeData from '../serealizeData';
import httpClient from '../../../utils/axios';
import { setNewStateUpdate } from '../../../utils';

const initialState = {
	evento_id: '',
	seccion_id: '',
	image_file: '',
	orden_visual: '',
	titulo_fecha: '',
	titulo_evento: '',
	nombre_recinto: '',
}

const AddPortal = ({ layaout, history, location }) => {
	const _uploapFileRef = useRef(null);
	const classes = UsethemeStyleGlobal();
	const [ eventos, setEventos ] = useState([]);
	const [ update, setUpdate ] = useState(false);
	const [ updateURI, setUpdateURI ] = useState('');
	const [ secciones, setSecciones ] = useState([]);
	const [ state, setState ] = useState(initialState);
	const [ uploadingFlag, setUploading ] = useState(true);

	useEffect(() => {
		httpClient.apiGet('admin/eventos/')
		.then(({ data }) => {
			// console.log(data);
			setEventos(data);
		})

		httpClient.apiGet('admin/secciones/')
		.then(({ data }) => {
			// console.log(data);
			setSecciones(data);
		})
		return () => {}
	}, [])

	useEffect(() => {
		const { evento, seccion } = qs.parse(location.search);
		if(evento && seccion) {
			setUpdate(true);
			let paramId = `evento=${evento}&seccion=${seccion}`;
			setUpdateURI(paramId);
			httpClient.apiGet(`admin/portal-eventos/editar-seccion?${paramId}`, {
				cancelToken: httpClient.cancelToken
			})
			.then(({ data }) => {
				if(!data.seccion) {
					toast({
						classes:`black-text yellow`,
						html: 'La sección que intenta editar no existe o fue eliminada'
					})

					history.replace(`${DASHBOARD_URL_ROOT}portal/agregar-seccion`);
				} else {
					setState((prevState) => ({
						...prevState,
						...setNewStateUpdate(data.seccion)
					}))
					setUploading(false);
				}
			})
			.catch((err) => {
				if (axios.isCancel(err)) {
					console.log("cancelled 1", err);
				} else {
					console.error(err);
				}
			})
		} else {
			setUploading(false);
		}
	}, [ location ])


	const handleInput = (e) => {
		const nameInput = e.target.name;
		let valueInput = e.target.value;
		console.log({ nameInput, valueInput })
		
		switch(nameInput) {
			case 'image_file':
				valueInput = e.target.files[0];
				console.log('es file', valueInput)
				break;
		}

		setState((prevState) => {
			const newState = { ...prevState };
			newState[nameInput] = valueInput
			return newState;
		});
	};

	const handleSubmit = async () => {
		const [totalErrors, errors] = validate(state, update);

		if(!totalErrors) {
			try {
				console.log('updateURI', updateURI)
				const URI = (update) ? `editar-seccion?${updateURI}` : 'agregar-seccion';
				const { data } = await httpClient.apiPost(
					`admin/portal-eventos/${URI}`,
					serealizeData(state, update, 'portal'),
					{ cancelToken: httpClient.cancelToken }
				);

				if(data.cod == 400) {
					for(const field in data.errors) {
						for(const err in data.errors[field]) {
							toast({
								html: data.errors[field][err],
								classes:`black-text yellow`
							});
						}
					}
				} else if(data.cod == 200) {
					_uploapFileRef.current.reset();
					toast({
						html: `${(update) ? 'Actualización exitosa' : 'Registro exitoso'}`,
						classes:`black-text green`
					});

					if(update) {
						setState((prevState) => ({
							...prevState,
							...setNewStateUpdate(data.seccion)
						}))
					} else {
						setState(initialState);
					}
				}
			} catch(err) {
				console.error(err);
				toast({
					classes: 'black-text red',
					html: 'Algo a ido mal. Por favor vuelve a intentar'
				})
			}
		} else {
			for(const err in errors) {
				toast({
					html: errors[err],
					classes:'red black-text'
				});

			}
		}
	}

	return((uploadingFlag) ? <Loading/> :
		<div className={cx(style.addWrapper, layaout.panelContent)} >
			<h3>
				{ (update) ? 'Editar sección' : 'Agregar una sección al portal' }
			</h3>
			<section className={cx(style.gridC2, style.grid, style.gridInputs)} >
				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='titulo_evento'
					value={state.titulo_evento}
					className={cx(classes.textField)}
					label={
						<span>Título del evento <span className="red-text">*</span></span>
					}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='titulo_fecha'
					value={state.titulo_fecha}
					className={cx(classes.textField)}
					label={
						<span>Subtitulo (fecha) <span className="red-text">*</span></span>
					}
				/>

				<TextField
					type='text'
					variant='outlined'
					onChange={handleInput}
					name='nombre_recinto'
					value={state.nombre_recinto}
					className={cx(classes.textField)}
					label={
						<span>Nombre del recinto <span className="red-text">*</span></span>
					}
				/>

				<TextField
					type='number'
					variant='outlined'
					onChange={handleInput}
					name='orden_visual'
					value={state.orden_visual}
					className={cx(classes.textField)}
					label={
						<span>Orden visual <span className="red-text">*</span></span>
					}
				/>

				{ !update &&
					<TextField
						select
						name='evento_id'
						variant='outlined'
						onChange={handleInput}
						value={state.evento_id}
						label={
							<span>Evento al que pertenece <span className="red-text">*</span></span>
						}
						className={cx(classes.selectField)}
					>
						{
							eventos.map((el) => (
								<MenuItem key={el.id} value={el.id} >{ el.nombre }</MenuItem>
							))
						}
					</TextField>
				}

				{ !update &&
					<TextField
						select
						name='seccion_id'
						variant='outlined'
						onChange={handleInput}
						value={state.seccion_id}
						label={
							<span>Seleccione una sección <span className="red-text">*</span></span>
						}
						className={cx(classes.selectField)}
					>
						{
							secciones.map((el) => (
								<MenuItem key={el.id} value={el.id} >{ el.descripcion }</MenuItem>
							))
						}
					</TextField>
				}

				<UploadFile
					name='image_file'
					onChange={handleInput}
					refReset={_uploapFileRef}
					label={
						<span>Imagen del evento <span className="red-text">*</span></span>
					}
				/>
			</section>

			<section className={style.submit}>
				<button
					className={cx('btn')}
					onClick={handleSubmit}
				>
				{ (update) ? 'editar' : 'agregar'}{' '}sección
				</button>
			</section>
		</div>
	);
}
export default AddPortal;