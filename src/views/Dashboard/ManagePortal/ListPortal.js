import React, { useState, useEffect } from 'react';
import qs from 'querystringify';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';

/* utils / config */
import httpClient from '../../../utils/axios';
import { DASHBOARD_URL_ROOT } from '../../../config';

/*components */
import Loading  from '../Loading';
import toast from '../../../utils/toast';
import TablePagination from '../../../components/TablePagination/TablePagination';

const ListPortal = ({ layaout, history }) => {
	const [ secciones, setSecciones ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ uploadingDeleteFlag, setUploadingDeleteFlag ] = useState(false);

	useEffect(() => {
		httpClient.apiGet('admin/portal-eventos')
		.then(({ data }) => {
			setSecciones(
				data.map((seccion) => {
					let { seccion_id, evento_id } = seccion;
					let id = seccion_id + evento_id;
					let idURI = `evento=${evento_id}&seccion=${seccion_id}`;
					return {
						...seccion,
						id,
						delete: () => deleteSeccion(idURI),
						update: () => updateSeccion(idURI),
						evento_nombre: seccion.Eventos.nombre,
					}
				})
			);
			setUploading(false);
		})
		return () => {}
	}, [])

	const updateSeccion = (id) => {
		history.push(`${DASHBOARD_URL_ROOT}portal/editar-seccion?${id}`);
	}

	const deleteSeccion = (id) => {
		setUploadingDeleteFlag(true);
		httpClient.apiPost(`admin/portal-eventos/eliminar-seccion?${id}`)
		.then(({ data }) => {
			toast({
				html: data.message,
				classes:`black-text ${(data.cod == 200) ? 'green' : 'yellow'}`
			});
			const idsTmp = qs.parse(id);
			setSecciones((prevState) =>
				prevState.filter((seccion) => {
					// id_entidad_eliminada =  evento_id + seccion_id
					// Hay que filtrar las secciones y remover la entidad recien eliminada
					return(
						seccion.evento_id == idsTmp.evento
						&& seccion.seccion_id == idsTmp.seccion
					) ? false : true;
				})
			)
			setUploadingDeleteFlag(false);
		})
	}

	return(
		<div className={cx(style.addWrapper)} >
			<h3>Listado de secciones</h3>
			{ (uploadingDeleteFlag) ?
					<Loading/> // Se está eliminando una entidad...
				: (secciones.length) ? // No se a creado ninguna cámara aún
					<TablePagination
						editable
						colSpan={5}
						rowBody={secciones}
						rowHead={[
							{
								id: 'titulo_evento',
								label: 'Título del evento',
							},
							{
								id: 'titulo_fecha',
								label: 'Subtitulo (fecha)',
							},
							{
								id: 'nombre_recinto',
								label: 'Nombre del recinto',
							},
							{
								id: 'orden_visual',
								label: 'Orden Visual',
							},
							{
								id: 'evento_nombre',
								label: 'Evento al que pertenece',
							},
							{
								id: 'seccion_id',
								label: 'Sección',
							}
						]}
					/>
				: (uploadingFlag) ? <Loading/> :
					<p className='center' >No se a creado ninguna sección aún</p>
			}
		</div>
	)
}

export default ListPortal;