const emptyString = (value) => (value && value.trim()) ? false : true

const validate = (values, update = false) => {
	const errors = {};
	let totalErrors = 0;

	if(update) {
		// Validaciones para la actualización una sección del portal
	} else {
		// Validaciones para el registro de una nueva sección del portal
		if (emptyString(values.titulo_evento)) {
			errors.titulo_evento = 'El título del evento es requerido';
			totalErrors++;
		}

		if (emptyString(values.titulo_fecha)) {
			errors.titulo_fecha = 'El subtitulo es requerido';
			totalErrors++;
		}

		if (!values.evento_id || emptyString(values.evento_id.toString())) {
			errors.evento_id = 'Debes elegir un evento';
			totalErrors++;
		}

		if (!values.seccion_id || emptyString(values.seccion_id.toString())) {
			errors.seccion_id = 'Debes elegir una sección';
			totalErrors++;
		}

		if (!values.image_file) {
			errors.image_file = 'La imagen es requerido';
			totalErrors++;
		}

		if (emptyString(values.orden_visual)) {
			errors.orden_visual = 'El orden visual es requerida';
			totalErrors++;
		}

		if (emptyString(values.nombre_recinto)) {
			errors.nombre_recinto = 'El nombre del recinto es requerido';
			totalErrors++;
		}
	}
	return [totalErrors, errors];
};

export default validate;
