const emptyString = (value) => (value && value.trim()) ? false : true

const validate = (values, update = false) => {
	const errors = {};
	let totalErrors = 0;

	if(update) {
		// Validaciones para la actualización de la función
	} else {
		// Validaciones para el registro de una nueva función
		if (emptyString(values.nombre)) {
			errors.nombre = 'El nombre de la función es requerido';
			totalErrors++;
		}

		if (!values.evento_id) {
			errors.evento_id = 'Debes elegir un evento';
			totalErrors++;
		}

		if(emptyString(values.fecha)) {
			errors.fecha = 'La fecha es requerida';
			totalErrors++;
		}

		if(emptyString(values.duracion_estimada)) {
			errors.duracion_estimada = 'La duración es requerida';
			totalErrors++;
		}
	}
	return [totalErrors, errors];
};

export default validate;
