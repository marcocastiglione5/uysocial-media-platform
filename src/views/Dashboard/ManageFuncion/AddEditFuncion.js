import React, { useRef, useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/*components */
import Loading  from '../Loading';
import { MenuItem, TextField } from '@material-ui/core';

/* utils */
import toast from '../../../utils/toast';
import validate from './validateFunctions';
import serealizeData from '../serealizeData';
import httpClient from '../../../utils/axios';
import {
	formatearHora,
	formatearFecha,
	setNewStateUpdate,
} from '../../../utils';

const initialState = {
	fecha: '',
	nombre: '',
	estado_id: '',
	evento_id: '',
	moneda_id: '',
	valor_entrada: '',
	duracion_estimada: '',
}

const AddEditFuncion = ({ layaout }) => {
	const params = useParams();
	const classes = UsethemeStyleGlobal();
	const [ eventos, setEventos ] = useState([]);
	const [ monedas, setMonedas ] = useState([]);
	const [ update, setUpdate ] = useState(false);
	const [ state, setState ] = useState(initialState);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ estadoFuncion, setEstadoFuncion ] = useState([]);

	// Fecha y duración actual de la función cuando se está editando
	const [ defaultValueFecha, setDefaultValueFecha ] = useState('');
	const [ defaultValueDuracion, setDefaultValueDuracion ] = useState('');

	useEffect(() => {
		httpClient.apiGet('admin/eventos/')
		.then(({ data }) => {
			// console.log(data);
			setEventos(data);
		})

		httpClient.apiGet('admin/monedas/')
		.then(({ data }) => {
			// console.log(data);
			setMonedas(data);
		})

		httpClient.apiGet('admin/estados/clase/entidad',
			{ cancelToken: httpClient.cancelToken }
		)
		.then(({ data }) => {
			setEstadoFuncion(data);
		})
		.catch((err) => {
			if (axios.isCancel(err)) {
				console.log("cancelled 3", err);
			} else {
				console.error(err);
			}
		})
		return () => {}
	}, [])

	useEffect(() => {
		if(params.id) {
			setUpdate(true);
			httpClient.apiGet(`admin/funciones/editar/${params.id}`, {
				cancelToken: httpClient.cancelToken
			})
			.then(({ data }) => {
				if(!data.funcion) {
					toast({
						classes:`black-text yellow`,
						html: 'La función que intenta editar no existe o fue eliminada'
					})

					history.replace(`${DASHBOARD_URL_ROOT}funciones/agregar`);
				} else {
					let fechaFormateada = formatearFecha(data.funcion.fecha);
					let horaFormateada = formatearHora(data.funcion.duracion_estimada);
					setDefaultValueFecha(fechaFormateada);
					setDefaultValueDuracion(horaFormateada);
					setState((prevState) => ({
						...prevState,
						...setNewStateUpdate(data.funcion),
						fecha: fechaFormateada,
						duracion_estimada: horaFormateada
					}))
					setUploading(false);
				}
			})
			.catch((err) => {
				if (axios.isCancel(err)) {
					console.log('cancelled 1', err);
				} else {
					console.error(err);
				}
			})
		} else {
			setUploading(false);
		}
	}, [ params ])


	const handleInput = (e) => {
		const nameInput = e.target.name;
		let valueInput = e.target.value;

		setState((prevState) => {
			const newState = { ...prevState };
			newState[nameInput] = valueInput
			return newState;
		});
	};

	const handleSubmit = async () => {
		const [totalErrors, errors] = validate(state, update);

		if(!totalErrors) {
			try {
				const URI = (update) ? 'editar/'+params.id : 'agregar';
				const { data } = await httpClient.apiPost(
					`admin/funciones/${URI}`,
					serealizeData(state, update, 'funcion'),
					{ cancelToken: httpClient.cancelToken }
				);

				if(data.cod == 400) {
					for(const field in data.errors) {
						for(const err in data.errors[field]) {
							toast({
								html: data.errors[field][err],
								classes:`black-text yellow`
							});
						}
					}
				} else if(data.cod == 200) {
					toast({
						html: `${(update) ? 'Actualización exitosa' : 'Registro exitoso'}`,
						classes:`black-text green`
					});

					if(update) {
						setState((prevState) => ({
							...prevState,
							...setNewStateUpdate(data.funcion),
							fecha: formatearFecha(data.funcion.fecha),
							duracion_estimada: formatearHora(data.funcion.duracion_estimada)
						}))
					} else {
						setState(initialState);
						setDefaultValueFecha('');
						setDefaultValueDuracion('');
					}
				}
			} catch(err) {
				console.error(err);
				toast({
					classes: 'black-text red',
					html: 'Algo a ido mal. Por favor vuelve a intentar'
				})
			}
		} else {
			for(const err in errors) {
				toast({
					html: errors[err],
					classes:'red black-text'
				});

			}
		}
	}

	return((uploadingFlag) ? <Loading/> :
		<div className={cx(style.addWrapper)} >
			<h3>
				{ (update) ? 'Editar función' : 'Crear una función' }
			</h3>
			<section className={cx(style.gridC2, style.grid, style.gridInputs)} >
				<TextField
					type='text'
					name='nombre'
					variant='outlined'
					value={state.nombre}
					onChange={handleInput}
					className={cx(classes.textField)}
					label={
						<span>Nombre de la función <span className="red-text">*</span></span>
						}
				/>

				<TextField
					type='number'
					variant='outlined'
					name='valor_entrada'
					onChange={handleInput}
					value={state.valor_entrada}
					label='Valor de la entrada'
					className={cx(classes.textField)}
				/>

				<TextField
					name='fecha'
					label='Fecha y hora'
					type='datetime-local'
					onChange={handleInput}
					defaultValue={defaultValueFecha}
					InputProps={{
						classes:{
							root: style.textFieldInput
						}
					}}
					InputLabelProps={{
						shrink: true,
					}}
				/>

				<TextField
					name='duracion_estimada'
					label='Duración estimada'
					type='time'
					onChange={handleInput}
					defaultValue={defaultValueDuracion}
					InputProps={{
						classes:{
							root: style.textFieldInput
						}
					}}
					InputLabelProps={{
						shrink: true,
					}}
				/>

				<TextField
					select
					name='evento_id'
					variant='outlined'
					onChange={handleInput}
					value={state.evento_id}
					label='evento al que pertenece'
					className={cx(classes.selectField)}
				>
					{
						eventos.map((el) => (
							<MenuItem key={el.id} value={el.id} >{ el.nombre }</MenuItem>
						))
					}
				</TextField>

				<TextField
					select
					name='moneda_id'
					variant='outlined'
					onChange={handleInput}
					value={state.moneda_id}
					label='Seleccione una moneda'
					className={cx(classes.selectField)}
				>
					{
						monedas.map((el) => (
							<MenuItem key={el.id} value={el.id} >{ el.descripcion }</MenuItem>
						))
					}
				</TextField>

				<TextField
					select
					variant='outlined'
					name='estado_id'
					onChange={handleInput}
					value={state.estado_id}
					label='Estado de la función'
					className={cx(classes.selectField)}
				>
					{
						estadoFuncion.map((el) => (
							<MenuItem key={el.id} value={el.id} >{ el.nombre }</MenuItem>
						))
					}
				</TextField>
			</section>

			<section className={style.submit}>
				<button
					className={cx('btn')}
					onClick={handleSubmit}
				>
					{ (update) ? 'editar' : 'crear'}{' '}función
				</button>
			</section>
		</div>
	);
}
export default AddEditFuncion;