import React, { useState, useEffect } from 'react';

/* style */
import cx from 'classnames';
import style from '../dashboard.css';
import UsethemeStyleGlobal from '../../../components/customHooks/UsethemeStyleGlobal';

/* utils / config */
import httpClient from '../../../utils/axios';
import { DASHBOARD_URL_ROOT } from '../../../config';

/*components */
import Loading  from '../Loading';
import toast from '../../../utils/toast';
import TablePagination from '../../../components/TablePagination/TablePagination';

const ListFuncion = ({ layaout, history }) => {
	const [ funciones, setFunciones ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);
	const [ uploadingDeleteFlag, setUploadingDeleteFlag ] = useState(false);

	useEffect(() => {
		httpClient.apiGet('admin/funciones')
		.then(({ data }) => {
			setFunciones(data.map((funcion) => ({
				...funcion,
				evento_nombre: funcion.Eventos.nombre,
				update: () => updateFuncion(funcion.id),
				delete: () => deleteFuncion(funcion.id),
			})));
			setUploading(false);
		})
		return () => {}
	}, [])

	const updateFuncion = (id) => {
		history.push(`${DASHBOARD_URL_ROOT}funciones/editar/${id}`);
	}

	const deleteFuncion = (id) => {
		setUploadingDeleteFlag(true);
		httpClient.apiPost(`admin/funciones/eliminar/${id}`)
		.then(({ data }) => {
			toast({
				html: data.message,
				classes:`black-text ${(data.cod == 200) ? 'green' : 'yellow'}`
			});
			setFunciones((prevState) =>
				prevState.filter((funcion) => {
					return funcion.id != id;
				})
			)
			setUploadingDeleteFlag(false);
		})
	}

	return(
		<div className={cx(style.addWrapper)} >
			<h3>Listado de funciones</h3>
			{ (uploadingDeleteFlag) ?
					<Loading/> // Se está eliminando una entidad...
				: (funciones.length) ? // No se a creado ninguna cámara aún
					<TablePagination
						editable
						rowBody={funciones}
						rowHead={[
							{
								id: 'nombre',
								label: 'Nombre',
							},
							{
								id: 'evento_nombre',
								label: 'Evento al que pertenece',
							},
							{
								id: 'fecha',
								label: 'Fecha',
							},
							{
								id: 'duracion_estimada',
								label: 'Duracion estimada',
							},
							{
								id: 'can_entradas_vendida',
								label: 'Cantidad de entradas vendidas',
							},
							{
								id: 'valor_entrada',
								label: 'Valor de la entrada',
							},
							{
								id: 'estado_id',
								label: 'Estado',
							},
						]}
					/>
				: (uploadingFlag) ? <Loading/> :
					<p className='center' >No se a creado ninguna función aún</p>
			}
		</div>
	)
}

export default ListFuncion;