import React, { useEffect, useState } from 'react';
import slugify from 'slugify';
import { Link } from 'react-router-dom';

/* style */
import cx from 'classnames';
import UseStyle from './UseStyle.js';
import style from './misEntradas.css';

const Entrada = ({ detailEntrada }) => {
	const activa = detailEntrada.acceso_confirmado;
	const fecha = new Date(detailEntrada.creacion);

	return(
		<section className={style.detalleCompraEntrada} >
			<p className={style.textLabel} >Código de acceso:</p>
			<p className={style.textValue} >{ detailEntrada.codigo_acceso }</p>

			<p className={style.textLabel} >Estado de la entrada:</p>
			<p className={style.textValue} >{
				`Entrada ${(activa) ? 'activada' : 'sin activar'}`
			}</p>

			<p className={style.textLabel} >Fecha de compra:</p>
			<p className={style.textValue} >{
				fecha.getDate()+'/'+
				(fecha.getMonth() + 1)+'/'+
				fecha.getFullYear().toString().slice(-2)+' '+
				('0' + fecha.getHours()).slice(-2)+':'+
				('0' + fecha.getMinutes()).slice(-2)
			}</p>
		</section>
	)
}

export default Entrada;
