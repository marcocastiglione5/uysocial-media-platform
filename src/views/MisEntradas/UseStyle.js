import { makeStyles } from '@material-ui/core/styles';

// Paleta de colores de oficial
const firstColor = '#f19122';
const thirdColor = '#7c4d95';
const secondColor = '#d00e5a';

const MTRR = '.MuiTableRow-root';
const MTCR = '.MuiTableCell-root';
const MTBR = '.MuiTableBody-root';

const UseStyles = makeStyles(theme => ({
	mainTable: {
		minWidth:'inherit !important',
		[theme.breakpoints.down('sm')]: {
			display: 'block !important',
			[`& > ${MTBR}`]: {
				display: 'block !important',
			},
			[`& > ${MTBR} > ${MTRR}`]: {
				display: 'block !important',
			},
			[`& > ${MTBR} > ${MTRR} > ${MTCR}`]: {
				display: 'block !important',
			}
		}
	},
	tableDetalleCompra: {
		minWidth:'inherit !important',
	},
	accordionSummary: {
		padding: 0
	},
	collapseEntrada: {
		marginTop: 20,
	}
}))

export default UseStyles;