import React, { useEffect, useState } from 'react';
import slugify from 'slugify';
import { Link, useHistory } from 'react-router-dom';

/* style */
import cx from 'classnames';
import UseStyle from './UseStyle.js';
import style from './misEntradas.css';

/* utils */
import { log } from '../../utils/';
import { ASSETS_URL } from '../../config.js';

/* CustomHooks */
import { useMobileDetector } from '../../components/customHooks/';

/* componets */
import Entrada  from './Entrada';
import { Collapse } from '@material-ui/core';
import { ButtonLight, ButtonGradient } from '../../components/Buttons/';
import TablePagination from '../../components/TablePagination/TablePagination';

const EntradasGroup = ({ entradas }) => {
	const classes = UseStyle();
	const history = useHistory();
	const mobileActive = useMobileDetector(768);
	const [ expandDetail, setExpandDetail ] = useState(false);

	const funcion = entradas[0].funcione;
	const evento = funcion.evento;
	const fecha = new Date(funcion.fecha);
	const entradaActiva = entradas.find((el) => el.acceso_confirmado);
	const entradasSinActivar = entradas.filter((el) => (
		el.acceso_confirmado != entradaActiva.id
	));

	return(
		<section className={style.entradaMainContent} >
			<p className={style.infoEvento} >
				{ evento.nombre }{' - '}{ evento.estado.id }
			</p>
			<p className={style.infoFuncion} >
				Función { funcion.nombre }{' - '}
				{
					fecha.getDate()+'/'+
					(fecha.getMonth() + 1)+'/'+
					fecha.getFullYear().toString().slice(-2)+' '+
					('0' + fecha.getHours()).slice(-2)+':'+
					('0' + fecha.getMinutes()).slice(-2)
				}
			</p>

			<div className={style.resumen} >
				<div className={style.thubmnail} >
					<img
						alt={evento.nombre}
						src={ASSETS_URL+evento.imagen_show_compra}
					/>
				</div>

				<div className={style.infoEntrada}>
					<p>
						CANTIDAD ENTRADAS: {entradas.length}
					</p>
					<p>
						{'VALOR TOTAL:'}{' '}
						{funcion.moneda.simbolo}{' '}
						{ funcion.valor_entrada * entradas.length}
					</p>

					<div className={style.botones} >
						<ButtonGradient
							onClick={() => {
								const funcionId = entradaActiva.funcione.id;
								const eventoId = entradaActiva.funcione.evento_id;
								history.push(`/visor?evento=${eventoId}&funcion=${funcionId}`)
							}}
						> Ir a la sala virtual </ButtonGradient>

						<Link to={`/evento/${evento.id}/${slugify(evento.nombre)}`} >
							<ButtonLight>Ir a la web del show</ButtonLight>
						</Link>

						<ButtonLight
							onClick={() => {
								setExpandDetail((prevState) => !prevState);
							}}
						>Ver detalle de compra</ButtonLight>

					</div>

					<Collapse
						unmountOnExit
						timeout='auto'
						in={expandDetail}
						className={cx(
							classes.collapseEntrada,
						)}
					>
					{(mobileActive) ?
						<TablePagination
							emptyStyle
							activateEmptyRows={false}
							tableClasses={classes.tableDetalleCompra}
							rowBody={
								entradasSinActivar.map((entrada) => {
									return {
										body: (
											<Entrada
												key={entrada.id}
												detailEntrada={entrada}
											/>
										),
										id: entrada.id,
									}
								})
							}
						/>
						:
						<TablePagination
							emptyStyle
							activateEmptyRows={false}
							tableClasses={classes.tableDetalleCompra}
							rowHead={[
								{
									id: 'codigo_acceso',
									label: 'Código de acceso'
								},
								{
									id: 'acceso_confirmado',
									label: 'Estado de la entrada'
								},
								{
									id: 'creacion',
									label: 'Fecha de compra'
								}
							]}
							rowBody={entradasSinActivar.map((entrada) => {
								const fecha = new Date(entrada.creacion);
								const creacion = 
									fecha.getDate()+'/'+
									(fecha.getMonth() + 1)+'/'+
									fecha.getFullYear().toString().slice(-2)+' '+
									('0' + fecha.getHours()).slice(-2)+':'+
									('0' + fecha.getMinutes()).slice(-2);

								return {
									creacion,
									id: entrada.id,
									codigo_acceso: entrada.codigo_acceso,
									acceso_confirmado: 'Entrada sin activar'
								}
							})}
						/>
					}
					</Collapse>
				</div>
			</div>
		</section>
	)
}

export default EntradasGroup