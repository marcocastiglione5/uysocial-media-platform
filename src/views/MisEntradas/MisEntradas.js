import React, { useEffect, useState } from 'react';
import slugify from 'slugify';
import { Link } from 'react-router-dom';

/* style */
import cx from 'classnames';
import UseStyle from './UseStyle.js';
import style from './misEntradas.css';

/* utils */
import { log } from '../../utils/';
import connect from '../../context/connect';
import { ASSETS_URL } from '../../config.js';

/* actions */
import { setEntradasUser } from '../../actions/loginAction';

/* componets */
import EntradasGroup  from './EntradasGroup';
import Preloader  from '../../components/Preloader';
import { ButtonGradient } from '../../components/Buttons/';
import TablePagination from '../../components/TablePagination/TablePagination';

const MisEntradas = ({ user, setEntradasUser, history }) => {
	const classes = UseStyle();
	const [ entradas, setEntradas ] = useState([]);
	const [ uploadingFlag, setUploading ] = useState(true);

	useEffect(() => {
		// Actualizar las entradas del usuario
		setEntradasUser();
	}, [])

	useEffect(() => {
		try {
			// IDs de la función a la que pertenece cada entrada (sin repetidos)
			// para agrupar las entradas por funciones
			const entradasGroupId = [];

			const arrayTmp = [];
			user.entradas.forEach((el) => {
				if(!(el.funcion_id in arrayTmp)) {
					arrayTmp[el.funcion_id] = true;
					entradasGroupId.push(el.funcion_id)
				}
			})
			// Entradas agrupadas por funciones
			const entradasAgrupadas = [];
			entradasGroupId.forEach((el) => {
				const group = user.entradas.filter((entrada) => {
					return entrada.funcion_id == el;
				})
				entradasAgrupadas.push(group);
			})
			setEntradas(entradasAgrupadas);
		} catch(err) {
			console.error(err);
			log({
				err,
				components:'misEntradas',
				errorParse: JSON.stringify(err),
			})
		}
		setUploading(false);
	}, [ user ])

	const loading = (
		<div className={style.loading}>
			<Preloader
				active
				size="big"
				color="blue"
			/>
		</div>
	);

	return(
		<div className={style.misEntradasContent} >
			<h1>Mis entradas</h1>
			<div className={style.lineTitle} ></div>
			{ (uploadingFlag) ? loading :
				(!entradas.length) ?
				<p>No posee entradas aún</p>
				: // else
				<TablePagination
					emptyStyle
					tableClasses={classes.mainTable}
					activateEmptyRows={false}
					rowBody={entradas.map((group, key) => {
						return {
							body: (
								<EntradasGroup
									key={key}
									entradas={group}
								/>
							),
							id: key,
						}
					})}
				/>
			}
		</div>
	)
}
const mapStateToProps = (store) => ({
	user: store.login.user,
});

const mapDispatchToProps = (dispatch) => ({
	setEntradasUser: () => setEntradasUser(dispatch),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(MisEntradas);
