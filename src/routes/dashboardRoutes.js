/**
 * Todos los componentes de este fichero serán renderizados
 * dentro del Dashboard. El acceso estará restringido
 * Dependiendo del rol del usuario logueado.
 */
import React from 'react';

/* constants */
import { DASHBOARD_URL_ROOT, DASHBOARD_INDEX } from '../config';

/* components */
const Overview = React.lazy(() => {
	return import(/* webpackChunkName: 'Overview' */ '../views/Dashboard/Overview')
});
const ListEvent = React.lazy(() => {
	return import(/* webpackChunkName: 'ListEvent' */ '../views/Dashboard/ManageEvents/ListEvent')
});
const AddEditEvent = React.lazy(() => {
	return import(/* webpackChunkName: 'AddEditEvent' */ '../views/Dashboard/ManageEvents/AddEditEvent')
});
const ListCamera = React.lazy(() => {
	return import(/* webpackChunkName: 'ListCamera' */ '../views/Dashboard/ManageCamera/ListCamera')
});
const ListPortal = React.lazy(() => {
	return import(/* webpackChunkName: 'ListPortal' */ '../views/Dashboard/ManagePortal/ListPortal')
});
const ListFuncion = React.lazy(() => {
	return import(/* webpackChunkName: 'ListFuncion' */ '../views/Dashboard/ManageFuncion/ListFuncion')
});
const AddEditFuncion = React.lazy(() => {
	return import(/* webpackChunkName: 'AddEditFuncion' */ '../views/Dashboard/ManageFuncion/AddEditFuncion')
});
const AddEditPortal = React.lazy(() => {
	return import(/* webpackChunkName: 'AddEditPortal' */ '../views/Dashboard/ManagePortal/AddEditPortal')
});
const AddEditCamera = React.lazy(() => {
	return import(/* webpackChunkName: 'AddEditCamera' */ '../views/Dashboard/ManageCamera/AddEditCamera')
});

export default [
	{
		exact: true,
		name: 'overview',
		path: DASHBOARD_URL_ROOT + DASHBOARD_INDEX,
		Component: Overview,
	},
	{
		exact: true,
		name: 'Listado de eventos',
		path: DASHBOARD_URL_ROOT + 'eventos',
		Component: ListEvent
	},
	{
		exact: true,
		name: 'Agregar un evento',
		path: DASHBOARD_URL_ROOT + 'eventos/agregar',
		Component: AddEditEvent
	},
	{
		exact: true,
		name: 'Editar evento',
		path: DASHBOARD_URL_ROOT + 'eventos/editar/:id',
		Component: AddEditEvent
	},
	{
		exact: true,
		name: 'Agregar funciones',
		path: DASHBOARD_URL_ROOT + 'funciones/agregar',
		Component: AddEditFuncion
	},
	{
		exact: true,
		name: 'editar función',
		path: DASHBOARD_URL_ROOT + 'funciones/editar/:id',
		Component: AddEditFuncion
	},
	{
		exact: true,
		name: 'Listado de funciones',
		path: DASHBOARD_URL_ROOT + 'funciones',
		Component: ListFuncion
	},
	{
		exact: true,
		name: 'Agregar seccion del portal',
		path: DASHBOARD_URL_ROOT + 'portal/agregar-seccion',
		Component: AddEditPortal
	},
	{
		exact: true,
		name: 'Listado de secciones del portal',
		path: DASHBOARD_URL_ROOT + 'portal/secciones',
		Component: ListPortal
	},
	{
		exact: true,
		name: 'editar seccion del portal',
		path: DASHBOARD_URL_ROOT + 'portal/editar-seccion',
		Component: AddEditPortal
	},
	{
		exact: true,
		name: 'Agregar cámaras',
		path: DASHBOARD_URL_ROOT + 'camaras/agregar',
		Component: AddEditCamera
	},
	{
		exact: true,
		name: 'Agregar cámaras',
		path: DASHBOARD_URL_ROOT + 'camaras/editar/:id',
		Component: AddEditCamera
	},
	{
		exact: true,
		name: 'Listado de cámaras',
		path: DASHBOARD_URL_ROOT + 'camaras',
		Component: ListCamera
	}
]