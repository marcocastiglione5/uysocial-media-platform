We are conscious that users are worried about:
one centralized entity knowing and owning their personal information and a centralized censorship entity that restricts community freedom
buying products on social networks without having an escrow service for their funds
not being able to use cryptocurrencies to buy products and services

So, we want to go back to the internet first days' decentralization promise, we want to provide escrow services, and we want to offer a simple way to buy using crypto.

﻿Uy!'s team is working in a fun, useful and friendly decentralized ecosystem that will improve how users have fun and buy things on the web and how community puts its own limits.

In Uy! we will offer a self-regulated social network, media platform, and marketplace. Three appreciated features in one place where the total result is much bigger than the sum of each component.

Uy! propose:

Nobody owns users personal information
Censorship rules updated periodically by Uy!´s DAO. A group of CONTENT GUARDIANS will challenge not allowed content with economic incentives and a decentralized dispute solving mechanism. Any user can be a CONTENT GUARDIAN.
Utility token called SUY that creates incentives to be nice and popular and take care of content.
Companies that want to advertise on the platform will inject SUY into the treasury to distribute among users.
LIKES from other users give power to earn more SUY. Incentive to be nice.
Amount of SUY in stake gives power to earn more SUY. Incentive to save.